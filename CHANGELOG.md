# 1.0.1
## Corrections
 - Correction du "bug BMI" : les bons de livraison JIRA peuvent maintenant être créés si l'utilisateur a un jira paramétré en français.
 
# 1.0
## Nouvelle feature
 - Ajout d'une fonctionnalité de recherche des nouvelles mises à jour de l'outil nash-tools !
 
# 0.22
## Corrections et améliorations
 - Amélioration de la stabilité des fonctionnalités JIRA.
 - Correction de divers problème avec les préférences.

# 0.21
## Nouvelle feature
 - Ajout d'une option pour générer un bon de livraison dans JIRA lors de la création d'un package.
 
# 0.20
## Nouvelle feature
 - Ajout d'une option pour purger les services sur le serveur sauf les services publiés.

# 0.19
## Nouvelle feature
 - Ajout d'une option pour publier les services au moment de l'upload.

## Corrections et améliorations
 - Correction du build des services qui ont plusieurs steps mais qui ne les repartissent pas dans des sous dossiers.
 - Il est maintenant possible de générer des packages avec des services qui portent le même nom (le nom du répertoire dans lequel se trouve le service).

# 0.18
## Nouvelle feature
 - Ajout d'une commande pour générer un fichier d'extraction des chaines de caractère.

## Corrections et améliorations
 - Amélioration du build des services qui ne disposent pas de sous dossier.
 
# 0.17
## Nouvelles features
 - Ajout d'une option pour sauvegarder plusieurs profiles d'upload.

## Corrections et améliorations
 - Amélioration des performances lors du chargement du workspace.


# 0.16
## Nouvelles features
 - Ajout d'une option pour visualiser directement la formalité après un upload.


# 0.15
## Nouvelles features
 - Prise en compte des fichiers roles, meta et readme

# 0.14

## Nouvelles features
### Merge de formalité 
 - Il est maintenant possible de merger deux formalité selectionner la formalité et "tools->merge->after" pour ajouter les étapes d'une formalité à la suite des étapes de celle de base !
 - Fonctionne de la même maniere avec "tools->merge->before" mais pour ajouter les étapes au début de la formalité de base.

### Group & author
 - Une option dans le menu des préferences ajoute la possibilité de renseigner un group pour les uploads. Laissez le vide pour utiliser le group par défaut. 
 - Il est possible de renseigner un group également à la génération d'un package. il sera appliqué à tous les services.
 - De la même façon, une option dans le menu des préferences ajoute la possibilité de renseigner un author pour les upload. Laissez le vide pour utiliser l'author par défaut. 
 
## Corrections
 - Divers bugs d'affichage
 
# 0.13

## Nouvelles features
 - Un petit compteur s'affiche pour indiquer le nombre de services selectionnées
 - Le mode de representation du workspace est également affiché en bas de la fenêtre
 - Il est maintenant possible de créer un package avec uniquement des services à dépublier
 
## Corrections
 - La vue hiérarchique fonctionne correctement à présent.

# 0.12

## Nouvelles features

### Generation Regent
 - Il est maintenant possible de générer une step de regent pour les services selectionnés (tools->Generate regent, ou CTRL+R).

### Case à cocher
 - Il est maintenant possible de cocher simultanément plusieurs case à cocher en cochant la case d'un niveau superieur.

## Corrections
 - La reinstalation de nash-tools n'efface plus les anciens paramètres de l'utilisateur.
 
# 0.11

## Nouvelles features

### Upgrade de l'écran preferences
 - L'écran permet à présent de configurer toutes les urls, login et mot de passe pour toutes les fonctionnélité de nash-tools.
 
### Contrôle sur les packages
 - L'application remontra une erreur lorsqu'on essaye de créer un package avec deux services qui ont le même referenceId.

## Corrections
 - La sauvegarde des services fonctionne maintenant correctement en mode d'affichage "XML".

# 0.10

## Nouvelles features

### Dépublication
 - L'écran de création de package propose maintenant une fonctionnalité pour dépublier une formalité. 

### Jira (BETA) :
 - Possibilité d'afficher les information d'une fiche Jira (le champ "reference" de la fiche Jira doit être le reference-id de la formalité).
 - Ajout d'une option qui ferme les fiches jira lors de la génération d'un package.

### Nouvel écran 
 - Les préférences : Permet d'éditer la configuration directement dans l'interface.

# 0.9

## Nouvelles features
### refonte de l'upload
- Ajout d'une barre de progresson lors de l'upload d'une formalité
- Possibilité d'annuler l'action d'upload quand il y a plusieurs services à uploader (il est en revanche impossible d'annuler l'upload en cours).
- Le programme ne se bloque plus lors de l'upload et il est maintenant possible de réaliser d'autre action en attendant que l'upload soit terminé.

## Corrections
- Correction d'un bug qui pouvait faire planter la generation selenium si le data.xml a une balise default vide (en attendant la mise en place d'un contôle au build).

# 0.8

## Nouvelles features
- Ajout du choix des sources (tools->source) :
	- zip : le programme affiche les services au format zip.
	- xml : le programme affiche les services à partir des fichiers sources.
- Déplacement du fichier de configuration dans %USER%/Appdata/local/nash-tools.

## Corrections
- Correction d'un bug qui empêchait le menu du clic droit d'apparaitre sur certain poste.
- Correction d'un bug qui faisait planter le programme sur certain poste quand le user n'était pas administrateur.
