Bienvenu sur la documentation nash-tools !
==========================================

[![](https://readthedocs.org/projects/nash-tools/badge/?version=latest)](https://nash-tools.readthedocs.io/?badge=latest)

Installation et changelog
=========================

le fichier d'installation et le changelog se trouve sur git dans le projet nash-tools : 

[Releases nash-tools](https://gitlab.com/guichet-entreprises.fr/tools/nash-tools/-/releases)

<a name="Configuration"></a>Configuration
-------------

le fichier de configuration est installé dans le dossier personnel de l'utilisateur :

    C:\Users\michel\AppData\Local\nash-tools\nash-tools.conf


Utilisation en mode console
===========================

Usage
-----

    usage: nash_tools.py [-h] [--windows {yes,no}] [--verbose] [--upload]
      [--build] [--browser] [--selenium] [--translate]
      filename [filename ...]

This program take a list of standalone forms and convert them to a selenese
test case script or upload them to a forge.

Positional arguments
--------------------

    filename            list of filenames.

Optional arguments
------------------

    --help, -h          show this help message and exit
    --windows {yes,no}  Define if we need all popups windows
    --verbose, -v       Put the logging system on the console for info
    --upload, -u        upload a form
    --build, -b         build a form
    --browser, -B       open a browser if upload
    --selenium, -s      generate a selenium test
    --translate, -t     prepare a translate file

Utilisation de l'outil dans l'explorer windows
==============================================

À partir des source d'un service (description.xml)
--------------------------------------------------

un clic droit sur un fichier description.xml affiche un menu nash-tools avec les option suivantes :
* *Build* : Construit le service à partir des informations présentes dans le fichier description.xml. Si le logiciel detecte une anomalie de structure, le programe l'affichera dans une boite de dialogue (le service est quand même généré).
* *Build and upload* : build le service et l'upload vers la forge nash déclarée dans le fichier de [configuration](#Configuration). un login/mdp peux etre nécessaire, dans ce cas une fenetre s'affichera pour les renseigner.
* *Build, upload and Browse* : build le service et l'upload vers la forge nash déclarée dans le fichier de [configuration](#Configuration). Si le service est correctement téléchargé, le navigateur par défaut s'ouvrira sur une instance de ce service.

À partir d'un service au format zip
-----------------------------------

un clic droit sur un service au format zip affiche un menu nash-tools avec les option suivantes :
* *Upload* : upload le service vers la forge nash.
* *Generate Selenium* : Genere un test selenium a partir d'un dossier utilisateur. Le dossier doit déjà avoir été rempli et téléchargé.

Utilisation de l'IHM nash-tools
===============================

Générer un package
------------------

double clic sur l'icone nash-tools-ide sur le bureau ouvre une fenetre qui permet de selectionner son workspace, c'est a dire le dossier à partir duquel nash-tools affichera les services contenu dans ce dossier.
le menu permet plusieurs actions :

### File
* open workspace : change le workspace courant.
* save : enregistre le service courant, pour l'instant le fichier description.xml

### Tools
* upload : upload le service sur l'environnement parametré
* generate selenium : génère un test selenium pour chaque services coché dans l'explorateur de service. disponible uniquement lorsque l'option source est "ZIP"
* generate package : génère un package qui peut ensuite etre livré sur nexus grace à la case a cocher du même nom (necessite un login/mdp). Si la case n'est pas chochée, le package peut etre sauvegardé en local. le champ application propose deux choix : GE et GQ il est cependant editable. 
Si la case  "générer un BL Jira" est cochée, un bon de livraison sera généré dans le projet Jira selectionné. Il comprendra toutes les informations sur la version et/ou l'url de l'emplacement sur le dépot (nexus). pour utiliser cette fonctionnalité, les informations sur JIRA doivent etre renseignés dans les paramètres. 
L'interface de generation de package se présente de la maniere suivante :

![générer un package](images/package.png)

* Upload : Upload les services sélectionnés sur le nash configuré.
* Purge : Supprime les services sélectionnés sur le serveur sauf les services publiés (utile pour faire de la place en supprimant les version de developpement obsolète)
* String xternalization : Génère un fichier qui contient les chaines pour éventuelement gérer la tractions des services. Le fichier généré est placé dans le service.
* generate Regent : Ajoute une step de regent aux services selectionnés en faisant appel au WS de génération du Regent.

![générer une step regent](images/regent.png)

* Merge : Ajoute une ou plusieurs étapes *avant* ou *apres* toutes les étapes des services séléctionnés. Ce qui permet de faire des fusions de services tres facilement. Si l'étape que l'on souhaite ajoutée existe déjà, la nouvelle remplace l'ancienne.

### Window
* directory presentation : affiche tous les services les uns en dessous des autres (flat, par défaut) ou en mode hierarchique (hierarchical, en fonction de leurs position dans le workspace)
* source : affiche les fomalités au format zip contenu dans le workspace ou affiche les services à partir des sources (description.xml)
* preferences : configurer les url et les login mdp pour les fonctionnalités d'upload, de package, de regent et pour Jira

![préferences](images/prefs.png)
