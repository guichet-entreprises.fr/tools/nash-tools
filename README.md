Nash-tools for Guichet Entreprises
=========================

Bienvenu sur la documentation nash-tools !

     _   _           _           _              _     
    | \ | | __ _ ___| |__       | |_ ___   ___ | |___ 
    |  \| |/ _` / __| '_ \ _____| __/ _ \ / _ \| / __|
    | |\  | (_| \__ \ | | |_____| || (_) | (_) | \__ \
    |_| \_|\__,_|___/_| |_|      \__\___/ \___/|_|___/



# Installation

You can download the latest version of nash tools GUI here : https://gitlab.com/guichet-entreprises.fr/tools/nash-tools/-/releases

# Configuration

The conf file is located in the user's folder :

    C:\Users\michel\AppData\Local\nash-tools\nash-tools.conf

# Usage

    usage: nash_tools.py [-h] [--windows {yes,no}] [--verbose] [--upload]
                     [--build] [--browser] [--selenium] [--translate]
                     filename [filename ...]

This program take a list of standalone forms and convert them to a selenese
test case script or upload them to a forge.

# Positional arguments

    filename            list of filenames.

# Optional arguments

    -h, --help          show this help message and exit
    --windows {yes,no}  Define if we need all popups windows.
    --verbose, -v       Put the logging system on the console for info.
    --upload, -u        upload a form
    --build, -b         build a form
    --browser, -B       open a browser if upload
    --selenium, -s      generate a selenium test
    --translate, -t     prepare a translate file
    

# Example

Build a form, upload et browse :

    >nash-tools -uB C:\myfom\description.xml

Generate a translation file :

    >nash-tools -t C:\myfom\description.xml
