;---------------------------------
;  General
;---------------------------------
!addincludedir "./nsh"
!include "MUI2.nsh"
!include "StrFunc.nsh"
!include "fileassoc.nsh"

;---------------------------------
; The product
;---------------------------------
!define PRODUCT_SHORTNAME "nash-tools"
!define PRODUCT_LONGNAME "nash-tools"
!define PRODUCT_COMPANY "ge.fr"
!define PRODUCT_VERSION "1.0"

!define BN_PKG "${PRODUCT_SHORTNAME}"
!include "build_number_increment.nsh"

;---------------------------------
; Explorer context and registry
;---------------------------------
!define DESCRIPTION "nash-tools"
!define MENU_DESCRIPTION "nash-tools"
!define EXT "xml"
!define EXT_ZIP "zip"
!define FILECLASS "ge.nash-tools.dir"

;---------------------------------
; General
;---------------------------------
!define /date TIMESTAMP "%Y-%m-%d"

;---------------------------------
Name "${PRODUCT_LONGNAME}"
OutFile "setup_${PRODUCT_SHORTNAME}-v${PRODUCT_VERSION}-[${Build_NUMBER}]-${TIMESTAMP}.exe"
ShowInstDetails "nevershow"
ShowUninstDetails "nevershow"
CRCCheck on
XPStyle on
VIProductVersion "${PRODUCT_VERSION}-[${Build_NUMBER}]"
SpaceTexts none

;---------------------------------
!define MUI_ICON "icon/ge.ico"
!define MUI_UNICON "icon/ge.ico"
BrandingText "Guichet Entreprises - ${TIMESTAMP}"

;--------------------------------
;Folder selection page
InstallDir "$PROGRAMFILES\ge.fr\${PRODUCT_SHORTNAME}"
InstallDirRegKey HKCU "Software\${PRODUCT_SHORTNAME}" ""

;--------------------------------
;Modern UI Configuration

!insertmacro MUI_PAGE_DIRECTORY

!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
;--------------------------------
!insertmacro MUI_LANGUAGE "French"

Var XmlFileClass
Var XmlFileUserClass
Var ZipFileClass

;-------------------------------- 
;Installer Sections     
Section "install"  
	;Add files
	SetOutPath "$INSTDIR"

 	File /r "..\\dist\\*.*"

	ReadRegStr $XmlFileClass HKCR ".${EXT}" ""
	ReadRegStr $ZipFileClass HKCR ".${EXT_ZIP}" ""
	
 	;MessageBox MB_OK "Class : $XmlFileUserClass"
	;SetRegView 64
	WriteRegStr 		HKCR "$XmlFileClass\shell\${PRODUCT_SHORTNAME}" "Icon" `$INSTDIR\${PRODUCT_SHORTNAME}.exe,0`
	WriteRegStr 		HKCR "$XmlFileClass\shell\${PRODUCT_SHORTNAME}" "ExtendedSubCommandsKey" "xmlfile\nashtools.menu"
	
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\build" "MUIVerb" "Build"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\build" "icon" "$INSTDIR\img\build.ico"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\build\command" "" `"$INSTDIR\nash-tools.exe" -vb "%1"`
	
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\upload" "MUIVerb" "Build and Upload"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\upload" "icon" "$INSTDIR\img\upload.ico"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\upload\command" "" `"$INSTDIR\nash-tools.exe" -vu "%1"`

	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\browse" "MUIVerb" "Build, Upload and Browse"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\browse" "icon" "$INSTDIR\img\browse.ico"
	WriteRegStr 		HKCR "$XmlFileClass\nashtools.menu\Shell\browse\command" "" `"$INSTDIR\nash-tools.exe" -vuB "%1"`
	
	WriteRegStr 		HKCR "*\shell\${PRODUCT_SHORTNAME}" "Icon" `$INSTDIR\${PRODUCT_SHORTNAME}.exe,0`
	WriteRegStr 		HKCR "*\shell\${PRODUCT_SHORTNAME}" "ExtendedSubCommandsKey" "xmlfile\nashtools.menu"
	
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\build" "MUIVerb" "Build"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\build" "icon" "$INSTDIR\img\build.ico"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\build\command" "" `"$INSTDIR\nash-tools.exe" -vb "%1"`
	
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\upload" "MUIVerb" "Build and Upload"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\upload" "icon" "$INSTDIR\img\upload.ico"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\upload\command" "" `"$INSTDIR\nash-tools.exe" -vu "%1"`

	WriteRegStr 		HKCR "*\nashtools.menu\Shell\browse" "MUIVerb" "Build, Upload and Browse"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\browse" "icon" "$INSTDIR\img\browse.ico"
	WriteRegStr 		HKCR "*\nashtools.menu\Shell\browse\command" "" `"$INSTDIR\nash-tools.exe" -vuB "%1"`
	
	WriteRegStr 		HKCR "txtfile\shell\${PRODUCT_SHORTNAME}" "Icon" `$INSTDIR\${PRODUCT_SHORTNAME}.exe,0`
	WriteRegStr 		HKCR "txtfile\shell\${PRODUCT_SHORTNAME}" "ExtendedSubCommandsKey" "xmlfile\nashtools.menu"
	
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\build" "MUIVerb" "Build"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\build" "icon" "$INSTDIR\img\build.ico"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\build\command" "" `"$INSTDIR\nash-tools.exe" -vb "%1"`
	
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\upload" "MUIVerb" "Build and Upload"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\upload" "icon" "$INSTDIR\img\upload.ico"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\upload\command" "" `"$INSTDIR\nash-tools.exe" -vu "%1"`

	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\browse" "MUIVerb" "Build, Upload and Browse"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\browse" "icon" "$INSTDIR\img\browse.ico"
	WriteRegStr 		HKCR "txtfile\nashtools.menu\Shell\browse\command" "" `"$INSTDIR\nash-tools.exe" -vuB "%1"`

	WriteRegStr 		HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.xml" "" "xmlfile"
	WriteRegStr 		HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.xml" "PerceivedType" "text"
	WriteRegStr 		HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.xml" "Content Type" "text/xml"
	
	StrCmp $ZipFileClass "" notfound done
	notfound:
		WriteRegStr 		HKCR ".${EXT_ZIP}" "" "CompressedFolder"
		ReadRegStr $ZipFileClass HKCR ".${EXT_ZIP}" ""
	done:
	WriteRegStr 		HKCR "$ZipFileClass\shell\${PRODUCT_SHORTNAME}" "Icon" `$INSTDIR\${PRODUCT_SHORTNAME}.exe,0`
	WriteRegStr 		HKCR "$ZipFileClass\shell\${PRODUCT_SHORTNAME}" "ExtendedSubCommandsKey" "$ZipFileClass\nashtools.menu"
	
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\selenium" "MUIVerb" "Generate Selenium"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\selenium" "icon" "$INSTDIR\img\selenium.ico"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\selenium\command" "" `"$INSTDIR\nash-tools.exe" -vs "%1"`
	
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\Upload" "MUIVerb" "Upload"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\Upload" "icon" "$INSTDIR\img\upload.ico"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\Upload\command" "" `"$INSTDIR\nash-tools.exe" -vu "%1"`
	
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\browse" "MUIVerb" "Upload and Browse"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\browse" "icon" "$INSTDIR\img\browse.ico"
	WriteRegStr 		HKCR "$ZipFileClass\nashtools.menu\Shell\browse\command" "" `"$INSTDIR\nash-tools.exe" -vuB "%1"`
	;DeleteRegKey 		HKCU "Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.xml"
	
	WriteUninstaller "$INSTDIR\Uninstall.exe"
 
	;create desktop link and start-menu items
	CreateShortCut "$DESKTOP\${PRODUCT_SHORTNAME}-ide.lnk" "$INSTDIR\nash-tools-ide.exe" ""
    CreateDirectory "$SMPROGRAMS\${PRODUCT_COMPANY}\${PRODUCT_SHORTNAME}"
    CreateShortCut "$SMPROGRAMS\${PRODUCT_COMPANY}\${PRODUCT_SHORTNAME}\Uninstall.lnk" "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
    CreateShortCut "$SMPROGRAMS\${PRODUCT_COMPANY}\${PRODUCT_SHORTNAME}\${PRODUCT_SHORTNAME}-ide.lnk" "$INSTDIR\nash-tools-ide.exe" "" "$INSTDIR\nash-tools-ide.exe" 0

	
SectionEnd
 
 
;--------------------------------    
;Uninstaller Section  
Section "un.Uninstall"
 
	;Delete Files 
	RMDir /r "$INSTDIR\*.*"    

	;Remove the installation directory
	RMDir "$INSTDIR"

	ReadRegStr $XmlFileClass HKCR ".${EXT}" ""
	DeleteRegKey HKCR "*\shell\${PRODUCT_SHORTNAME}"
	DeleteRegKey HKCR "*\nashtools.menu"
	StrCmp $XmlFileClass "" notfoundxml
		DeleteRegKey HKCR "$XmlFileClass\nashtools.menu"
		DeleteRegKey HKCR "$XmlFileClass\shell\${PRODUCT_SHORTNAME}"
		Goto donexml
	notfoundxml:
		; ----
	donexml:
	
	
	ReadRegStr $ZipFileClass HKCR ".${EXT_ZIP}" ""
	StrCmp $ZipFileClass "" notfoundzip
		DeleteRegKey HKCR "$ZipFileClass\nashtools.menu"
		DeleteRegKey HKCR "$ZipFileClass\shell\${PRODUCT_SHORTNAME}"
		Goto donezip
	notfoundzip:
		; ----
	donezip:
	
	;Delete Start Menu Shortcuts
  Delete "$DESKTOP\${PRODUCT_SHORTNAME}-ide.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_COMPANY}\${PRODUCT_SHORTNAME}\*.*"
  RmDir  "$SMPROGRAMS\${PRODUCT_COMPANY}\${PRODUCT_SHORTNAME}"
  
SectionEnd
