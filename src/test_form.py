
from nash.entities import form
from nash.entities import xml_form


def main():
    formx = xml_form.XmlForm(
        r"C:\Users\Arnaud\Desktop\testnashtools"
        r"\divers\JQPA-PE1-PP2\description.xml")
    formxtomerge = xml_form.XmlForm(
        r"C:\Users\Arnaud\Desktop\testnashtools\divers\REGENT\description.xml")
    # print(formz)

    # formz = zip_form.ZipForm(
    #    r"C:\Users\Arnaud\Desktop\testnashtools\"
    #     "divers\TEST-TYPE-01\TEST-TYPE-01.zip")

    print(formx)
    print('\n' + '%s' %
          '\n'.join(str(step) for step in formx.get_steps()))
    print("---------------------------------------")
    formx.merge_form(formxtomerge, place=form.PLACE_END)
    print(formx)
    print('\n' + '%s' %
          '\n'.join(str(step) for step in formx.get_steps()))

    formx.write(full_save=True)


if __name__ == '__main__':
    main()
