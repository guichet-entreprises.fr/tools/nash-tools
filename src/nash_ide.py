﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import os
import ntpath
import logging
import datetime
import webbrowser
import wx.stc as stc
import wx.lib.scrolledpanel
import wx.lib.agw.customtreectrl as CT
from wx.lib.pubsub import pub
import wx.lib.inspection

from anytree import Node, find
from jira import JIRAError

from nash.entities import form, xml_form, zip_form
from nash.services import package, commons
from nash.services import standalone_form
from nash.services import jira_connection
from nash.gui.regent_dialog import RegentDialog
from nash.gui.package_dialog import PackageDialog
from nash.gui.progress_dialog import ProgressDialog
from nash.gui.login_dialog_ide import LoginDialogIde
from nash.thread.service_thread import ServiceThread
from nash.gui.jira_create_dialog import JiraCreateDialog
from nash.gui.preferences_dialog import PreferencesDialog

VERSION_IDE = "1.0.1"
ICON_DIRECTORY = 'img'


class NashIdeFrame(wx.Frame):
    """
    the main nash ide frame
    """
    # constants
    MODE_EXPLORER_HIERARCHICAL = 1
    MODE_EXPLORER_FLAT = 2

    # status text
    status_text_nb_form = ""
    status_text_source_xml = "Source - XML"
    status_text_source_zip = "Source - ZIP"

    # Workspace Tree
    workspace = None

    # list of all the standalone_forms in the workspace
    list_forms = None

    # the panels
    formality_tree = None
    search_field = None
    main_splitter = None
    formality_splitter = None
    formality_editor = None

    # MenuItems
    pres_hierarchical_item = None
    pres_flat_item = None

    # data to save
    content_not_saved = False
    current_form = {}

    # Connection JIRA
    use_jira = False

    current_pres = MODE_EXPLORER_HIERARCHICAL
    current_source = commons.SOURCE_XML

    def __init__(self, *args, **kw):
        # ensure the parent's __init__ is called
        super(NashIdeFrame, self).__init__(*args, **kw)

        if self.check_new_version():
            self.Close(True)
            return

        # create a menu bar
        self.make_menu_bar()

        # the status bar
        self.CreateStatusBar()
        self.SetStatusText("Welcome to nash IDE!")

        # icon
        icon = wx.Icon()
        icon.CopyFromBitmap(wx.Bitmap(os.path.join(
            ICON_DIRECTORY, "ge.ico"), wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)
        # affiche la fenetre d'inspection
        # wx.lib.inspection.InspectionTool().Show()

        self.__action__ = {}
        self.__action__['UPLOAD'] = self.on_upload
        self.__action__['STRING_EX'] = self.on_string_externalization
        self.__action__['REGENT'] = self.on_regent

        self.panel_explorer = None
        self.panel_formality = None
        self.panel_formality_infos = None

        self.last_upload_event = None

        self.init_conf()

        # ouverture du workspace
        self.on_open_workspace(None)

    def make_menu_bar(self):

        file_menu = wx.Menu()
        open_workspace_item = file_menu.Append(
            -1, "&Open workspace...\tCtrl-D",
            "ouvrir un repertoire contenant"
            " une arborescence de formalité")
        save_file_item = file_menu.Append(-1, "&Save\tCtrl-S",
                                          "ouvrir une formalité autonome"
                                          " au format zip")
        file_menu.AppendSeparator()
        # label
        exit_item = file_menu.Append(wx.ID_EXIT, 'Quit', 'Quit Nash IDE')

        help_menu = wx.Menu()

        about_item = wx.MenuItem(help_menu, wx.ID_ABOUT)
        help_menu.Append(about_item)

        help_item = wx.MenuItem(help_menu, wx.ID_HELP)
        help_menu.Append(help_item)

        # tools menu
        tools_menu = wx.Menu()

        # selenium
        self.selenium_item = wx.MenuItem(tools_menu, wx.ID_ANY,
                                         "&Generate Selenium\tCtrl-K",
                                         "générer un test selenium à partir"
                                         " de la formalité")
        self.selenium_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'selenium.ico'),
                     wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(self.selenium_item)
        if self.current_source == commons.SOURCE_XML:
            self.selenium_item.Enable(enable=False)

        # package
        package_item = wx.MenuItem(tools_menu, wx.ID_ANY, "&Generate Package\tCtrl-P",
                                   "générer un package à partir"
                                   " des services selectionnés")
        package_item.SetBitmap(wx.Bitmap(wx.Image(os.path.join(
            ICON_DIRECTORY, 'package.ico'), wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(package_item)

        # upload
        upload_item = wx.MenuItem(tools_menu, wx.ID_ANY, "&Upload\tCtrl-U",
                                  "Upload les services selectionnés vers"
                                  " le nash paramétré par défaut")
        upload_item.SetBitmap(wx.Bitmap(wx.Image(os.path.join(
            ICON_DIRECTORY, 'upload.ico'), wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(upload_item)
        # purge
        purge_item = wx.MenuItem(tools_menu, wx.ID_ANY, "&Purge\tCtrl-M",
                                 "Purge les services selectionnés sur le serveur"
                                 " sauf les services publiés")
        purge_item.SetBitmap(wx.Bitmap(wx.Image(os.path.join(
            ICON_DIRECTORY, 'purge.ico'), wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(purge_item)
        # regent
        self.regent_item = wx.MenuItem(tools_menu, wx.ID_ANY,
                                       "Generate &Regent\tCtrl-R",
                                       "Générer un fichier regent pour les"
                                       " services selectionnés")
        self.regent_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'regent.ico'),
                     wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(self.regent_item)

        # create issue jira
        self.create_jira_item = wx.MenuItem(tools_menu, wx.ID_ANY,
                                            "Create a &JIRA issue \tCtrl-J",
                                            "Créer une fiche Jira pour les"
                                            " services selectionnés")
        self.create_jira_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'jira.ico'),
                     wx.BITMAP_TYPE_ANY)))
        # tools_menu.Append(self.create_jira_item)

        # String externalization
        string_ex_item = wx.MenuItem(tools_menu, wx.ID_ANY, "&String externalization\tCtrl-E",
                                     "extrait les chaînes de caractère")
        string_ex_item.SetBitmap(wx.Bitmap(wx.Image(os.path.join(
            ICON_DIRECTORY, 'extern.ico'), wx.BITMAP_TYPE_ANY)))
        tools_menu.Append(string_ex_item)

        # merge menu TODO
        merge_menu = wx.Menu()
        merge_after_item = wx.MenuItem(merge_menu, wx.ID_ANY, 'after')
        merge_after_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'merge.ico'),
                     wx.BITMAP_TYPE_ANY)))
        merge_menu.Append(merge_after_item)

        merge_before_item = wx.MenuItem(merge_menu, wx.ID_ANY, 'before')
        merge_before_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'merge.ico'),
                     wx.BITMAP_TYPE_ANY).Rotate180()))
        merge_menu.Append(merge_before_item)

        tools_menu.Append(wx.ID_ANY, "&Merge", merge_menu)

        # window menu
        window_menu = wx.Menu()

        # presentation menu
        pres_menu = wx.Menu()
        pres_flat_item = wx.MenuItem(window_menu, wx.ID_ANY, 'Flat')
        pres_flat_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'flat.ico'),
                     wx.BITMAP_TYPE_ANY)))
        pres_menu.Append(pres_flat_item)

        pres_hierarchical_item = wx.MenuItem(
            window_menu, wx.ID_ANY, 'Hierarchical')
        pres_hierarchical_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'hierarchical.ico'),
                     wx.BITMAP_TYPE_ANY)))
        pres_menu.Append(pres_hierarchical_item)

        window_menu.Append(wx.ID_ANY, "&directory presentation", pres_menu)

        # source menu
        source_menu = wx.Menu()

        source_zip_item = wx.MenuItem(window_menu, wx.ID_ANY, 'ZIP')
        source_zip_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'zip.ico'),
                     wx.BITMAP_TYPE_ANY)))
        source_menu.Append(source_zip_item)

        source_xml_item = wx.MenuItem(window_menu, wx.ID_ANY, 'XML')
        source_xml_item.SetBitmap(wx.Bitmap(
            wx.Image(os.path.join(ICON_DIRECTORY, 'xml.ico'),
                     wx.BITMAP_TYPE_ANY)))
        source_menu.Append(source_xml_item)

        window_menu.Append(wx.ID_ANY, "&sources", source_menu)

        # preference item
        preferences_item = wx.MenuItem(
            window_menu, wx.ID_ANY,
            "&Preferences\tCtrl-m", "Jira preferences, etc.")
        window_menu.Append(preferences_item)

        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, "&File")
        menu_bar.Append(tools_menu, "&Tools")
        menu_bar.Append(window_menu, "&Window")
        menu_bar.Append(help_menu, "&Help")

        self.SetMenuBar(menu_bar)

        self.Bind(wx.EVT_MENU, self.on_save_file, save_file_item)
        self.Bind(wx.EVT_MENU, self.on_open_workspace, open_workspace_item)
        self.Bind(wx.EVT_MENU, self.on_upload, upload_item)
        self.Bind(wx.EVT_MENU, self.on_purge, purge_item)
        self.Bind(wx.EVT_MENU, self.on_regent, self.regent_item)
        self.Bind(wx.EVT_MENU, self.on_create_jira_issue,
                  self.create_jira_item)
        self.Bind(wx.EVT_MENU, self.on_generate_package, package_item)
        self.Bind(wx.EVT_MENU, self.on_generate_selenium, self.selenium_item)
        self.Bind(wx.EVT_MENU, self.on_string_externalization, string_ex_item)
        self.Bind(wx.EVT_MENU, self.on_pres_flat, pres_flat_item)
        self.Bind(wx.EVT_MENU, self.on_pres_hierarchical,
                  pres_hierarchical_item)
        self.Bind(wx.EVT_MENU, self.on_source_xml, source_xml_item)
        self.Bind(wx.EVT_MENU, self.on_source_zip, source_zip_item)
        self.Bind(wx.EVT_MENU, self.on_preferences, preferences_item)
        self.Bind(wx.EVT_MENU, self.on_exit, exit_item)
        self.Bind(wx.EVT_MENU, self.on_about, about_item)
        self.Bind(wx.EVT_MENU, self.on_help, help_item)

        self.Bind(wx.EVT_MENU, self.on_merge_after, merge_after_item)
        self.Bind(wx.EVT_MENU, self.on_merge_before, merge_before_item)

        pub.subscribe(self.on_result_progress, "result_progress")
        pub.subscribe(self.on_prompt_login, "promptLogin")

    def init_conf(self):
        conf = commons.get_conf()
        self.use_jira = commons.str2bool(conf[commons.JIRA]['use_jira'])

        if self.use_jira:
            j = jira_connection.JiraConnection()
            j.url = conf[commons.JIRA]["url"]
            j.user = conf[commons.JIRA]["user"]
            j.password = conf[commons.JIRA]["password"]
            try:
                j.get_connector()
            except JIRAError as ex:
                self.use_jira = False
                if ex.status_code == 403:
                    wx.MessageBox(
                        "error while connecting to Jira : Bad credentials",
                        "Jira", wx.OK | wx.ICON_ERROR)
                else:
                    wx.MessageBox("error while connecting to Jira : \n%s" % (
                        ex), "Jira", wx.OK | wx.ICON_ERROR)

    # set the number of checked formalities in the status text
    def set_status_text_nb_form_checked(self, nb_form_checked):
        self.status_text_nb_form = f'{nb_form_checked} formalities checked'
        self.set_status_text()

    def set_status_text(self):
        if self.current_source == commons.SOURCE_XML:
            source = self.status_text_source_xml
        else:
            source = self.status_text_source_zip

        self.SetStatusText(
            f'{source}, {self.status_text_nb_form}')

    # The dialog is automatically destroyed on exit from the context manager
    def make_panels(self):
        if self.main_splitter is None:
            self.main_splitter = wx.SplitterWindow(
                self, wx.ID_ANY, style=wx.SP_3D | wx.SP_BORDER)
            self.panel_explorer = wx.Panel(self.main_splitter, wx.ID_ANY)
            self.panel_formality = wx.Panel(self.main_splitter, wx.ID_ANY)

            self.panel_explorer.SetBackgroundColour(wx.Colour(255, 255, 255))
            self.panel_formality.SetBackgroundColour(wx.Colour(255, 255, 255))

            self.main_splitter.SplitVertically(
                self.panel_explorer, self.panel_formality, 300)
            main_sizer = wx.BoxSizer(wx.VERTICAL)
            main_sizer.Add(self.main_splitter, 1, wx.EXPAND)
            self.SetSizer(main_sizer)

    def make_viewer(self, my_form):
        if self.formality_editor is not None:
            self.formality_editor.Destroy()

        if self.formality_splitter is not None:
            self.formality_splitter.Destroy()

        if my_form is None:
            return

        # formality Splitter
        self.formality_splitter = wx.SplitterWindow(
            self.panel_formality, wx.ID_ANY, style=wx.SP_3D | wx.SP_BORDER)

        # Formality information view
        self.panel_formality_infos = wx.lib.scrolledpanel.ScrolledPanel(
            self.formality_splitter, wx.ID_ANY, style=wx.SIMPLE_BORDER)
        self.formality_editor = stc.StyledTextCtrl(
            self.formality_splitter, style=wx.SIMPLE_BORDER)
        self.formality_editor.SetMarginType(1, stc.STC_MARGIN_NUMBER)

        self.formality_editor.SetValue(my_form.get_description_xml_raw())

        self.current_form = my_form

        self.formality_splitter.SplitHorizontally(
            self.panel_formality_infos, self.formality_editor, 300)
        formality_splitter_sizer = wx.BoxSizer(wx.HORIZONTAL)
        formality_splitter_sizer.Add(
            self.formality_splitter, 1, wx.EXPAND | wx.ALL, 3)
        self.make_panel_formality_infos(my_form)
        self.panel_formality.SetSizer(formality_splitter_sizer)
        self.panel_formality.Layout()

    def make_panel_formality_infos(self, my_form):
        forms_infos = my_form.get_formality_infos()

        if self.use_jira:
            forms_infos.update(my_form.get_formality_infos_jira())

        static_box = wx.StaticBox(
            self.panel_formality_infos, wx.ID_ANY, "standalone form")
        static_box_sizer = wx.StaticBoxSizer(static_box, wx.VERTICAL)
        grid_sizer = wx.GridBagSizer(len(forms_infos.keys()), 2)
        i = 0
        for key, value in forms_infos.items():
            label = wx.StaticText(
                self.panel_formality_infos, label=key + " : ")
            grid_sizer.Add(label, (i, 0), flag=wx.ALIGN_RIGHT)
            val = wx.StaticText(self.panel_formality_infos, label=value)
            grid_sizer.Add(val, (i, 1))
            i += 1
        static_box_sizer.Add(grid_sizer, 0, wx.ALL | wx.LEFT, 5)
        self.panel_formality_infos.SetSizerAndFit(static_box_sizer)
        self.panel_formality_infos.SetupScrolling()

    def make_explorer(self, pathname, mode=MODE_EXPLORER_HIERARCHICAL):
        # create a panel in the frame
        if self.search_field is None:
            search_panel = wx.Panel(self.panel_explorer, wx.ID_ANY)
            self.search_field = wx.SearchCtrl(search_panel, size=(200, -1),
                                              style=wx.TE_PROCESS_ENTER)
            self.search_field.ShowCancelButton(True)
            self.search_field.SetSize(200, 25)

            search_sizer = wx.BoxSizer(wx.VERTICAL)
            search_sizer.Add(self.search_field, 0, wx.EXPAND)

            self.search_field.SetSizer(search_sizer)

            self.Bind(wx.EVT_SEARCHCTRL_SEARCH_BTN, self.on_search,
                      self.search_field)
            self.Bind(wx.EVT_TEXT_ENTER, self.on_search, self.search_field)
            self.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.on_search_cancel,
                      self.search_field)

        if self.formality_tree is None:
            self.formality_tree = CT.CustomTreeCtrl(
                self.panel_explorer, agwStyle=CT.TR_TOOLTIP_ON_LONG_ITEMS |
                CT.TR_DEFAULT_STYLE)
            self.formality_tree.SetBackgroundColour(wx.Colour(255, 255, 255))
            self.Bind(CT.EVT_TREE_ITEM_CHECKED,
                      self.on_item_checked, self.formality_tree)
        else:
            self.formality_tree.DeleteAllItems()

        if mode == self.MODE_EXPLORER_HIERARCHICAL:
            self.make_explorer_hierarchical(pathname)
            # self.make_explorerHierarchical(pathname)
        else:
            self.make_explorer_flat(pathname)

        self.Bind(wx.EVT_TREE_SEL_CHANGED,
                  self.on_sel_changed, self.formality_tree)

        sizer = wx.BoxSizer(wx.VERTICAL)
        # sizer.Add(search_panel, 0, wx.CENTER, 0)
        sizer.Add(self.formality_tree, 1, wx.EXPAND, 2)
        self.panel_explorer.SetSizer(sizer)

        # reinitialisation du compteur
        self.set_status_text_nb_form_checked(0)

    def make_explorer_flat(self, pathname):
        # création de l'arbo de l'explorer
        root = self.formality_tree.AddRoot(
            'Workspace (%s)' % pathname, ct_type=1)
        self.formality_tree.SetAGWWindowStyleFlag(
            agwStyle=CT.TR_TOOLTIP_ON_LONG_ITEMS | CT.TR_EXTENDED |
            CT.TR_AUTO_TOGGLE_CHILD)
        for my_form in self.list_forms:
            self.formality_tree.AppendItem(
                root, my_form.get_form_name(), ct_type=1, data=my_form)

        self.formality_tree.Expand(root)

    def make_explorer_hierarchical(self, pathname):
        # création de l'arbo de l'explorer
        root = self.formality_tree.AddRoot(
            'Workspace (%s)' % pathname, ct_type=1)
        self.formality_tree.SetAGWWindowStyleFlag(
            agwStyle=CT.TR_TOOLTIP_ON_LONG_ITEMS | CT.TR_DEFAULT_STYLE |
            CT.TR_AUTO_TOGGLE_CHILD)
        node_root = Node('Workspace (%s)' % pathname, customtree=root)
        for my_form in self.list_forms:
            data = my_form
            if ntpath.basename(my_form.get_filename()) == "description.xml":
                path_tmp = my_form.get_filename().replace(
                    os.sep + "description.xml", "")
            else:
                path_tmp = my_form.get_filename()

            if pathname == path_tmp:
                path_split = [os.path.basename(path_tmp)]
            else:
                path_split = os.path.normpath(
                    path_tmp.replace(pathname + os.sep, "")).split(os.sep)
            parent = node_root

            for path in path_split[0:-1]:
                existing_node = find(
                    parent, lambda node, p=path: node.name == p, maxlevel=2)
                if existing_node is not None:
                    parent = existing_node
                else:
                    node = Node(path, parent=parent,
                                customtree=self.formality_tree
                                .AppendItem(parent.customtree, path,
                                            ct_type=1))
                    parent = node
            tree = self.formality_tree.AppendItem(parent.customtree,
                                                  path_split[-1], ct_type=1, data=data)
            node = Node(path_split[-1], parent=parent, customtree=tree)
        self.formality_tree.Expand(root)

    def load_workspace(self, path, mode=commons.SOURCE_XML):
        logging.info('Loading workspace %s', path)
        self.workspace = path

        begin_time = datetime.datetime.now()
        bi_frame = wx.BusyInfo("Now loading, please wait", self)
        try:
            self.list_forms = standalone_form.get_formality_from_dir(
                self.workspace, mode)

        finally:
            del bi_frame
        end_time = datetime.datetime.now()
        logging.info('Finished. %d formalities loaded in %s sec',
                     len(self.list_forms), (end_time - begin_time).seconds)

        # create the panels
        self.make_panels()
        self.make_explorer(self.workspace)
        self.set_status_text_nb_form_checked(0)
        self.Refresh()

    def get_selected_forms(self, item_parent=None, selected_forms=None):
        if item_parent is None:
            item_parent = self.formality_tree.GetRootItem()

        if selected_forms is None:
            selected_forms = []

        child, cookie = self.formality_tree.GetFirstChild(item_parent)

        while child:

            if(self.formality_tree.IsItemChecked(child) and
               child.GetData() is not None):
                selected_forms.append(child.GetData())

            selected_forms = self.get_selected_forms(child, selected_forms)
            child, cookie = self.formality_tree.GetNextChild(
                item_parent, cookie)

        return selected_forms

    def on_exit(self, event):
        """Close the frame, terminating the application."""
        del event
        self.Close(True)

    def on_item_checked(self, event):
        del event
        self.set_status_text_nb_form_checked(len(self.get_selected_forms()))

    def on_purge(self, event, user="", password=""):
        self.last_upload_event = event
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "Purge",
                          wx.OK | wx.ICON_ERROR)
            return
        answer = wx.MessageBox(
            "Attention, la purge entraine la suppression de toutes les formalités "
            " séléctionnées sur le serveur configuré sauf celle publiés, voulez vous continuer ?",
            caption="Purge", style=wx.YES_NO | wx.CENTRE)

        if(answer == wx.NO):
            return

        try:
            conf_infos = commons.get_conf(commons.UPLOAD)
            if user is not "":
                conf_infos['user'] = user
                conf_infos['password'] = password
            list_selected_forms = []
            for my_form in selected_forms:
                list_selected_forms.append(my_form)

            dlg = ProgressDialog("Purge", nb_formality * 10 + 2)
            dlg.Show()
            ServiceThread(commons.PURGE, nb_formality,
                          list_selected_forms, conf_infos)

        except BaseException as ex:
            logging.error(str(ex))
            wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
            return

    def on_upload(self, event, user="", password=""):
        self.last_upload_event = event
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "Upload",
                          wx.OK | wx.ICON_ERROR)
            return
        try:
            conf_infos = commons.get_conf(commons.UPLOAD)
            if user is not "":
                conf_infos['user'] = user
                conf_infos['password'] = password
            list_selected_forms = []
            for my_form in selected_forms:
                list_selected_forms.append(my_form)

            dlg = ProgressDialog("Upload", nb_formality * 10 + 2)
            dlg.Show()
            ServiceThread(commons.UPLOAD, nb_formality,
                          list_selected_forms, conf_infos)

        except BaseException as ex:
            logging.error(str(ex))
            wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
            return

    def on_string_externalization(self, event):
        self.last_upload_event = event
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "String externalization",
                          wx.OK | wx.ICON_ERROR)
            return
        try:
            list_selected_forms = []
            for my_form in selected_forms:
                list_selected_forms.append(my_form)

            dlg = ProgressDialog("String externalization",
                                 nb_formality * 10 + 2)
            dlg.Show()
            ServiceThread(commons.STRING_EX, nb_formality,
                          list_selected_forms)

        except BaseException as ex:
            logging.error(str(ex))
            wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
            return

    def on_create_jira_issue(self, event):
        del event
        # self.lastRegentEvent = event -> en cas de login/mdp
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "Regent",
                          wx.OK | wx.ICON_ERROR)
            return

        conf = commons.get_conf()
        self.use_jira = commons.str2bool(conf[commons.JIRA]['use_jira'])

        if not self.use_jira:
            wx.MessageBox("Jira n'est pas connecté."
                          " Connectez le avec l'écran des préférences",
                          "Jira",
                          wx.OK | wx.ICON_ERROR)
            return
        with JiraCreateDialog(self, "Create a Jira Issue") as jira_create_dlg:
            if jira_create_dlg.ShowModal() == wx.ID_CANCEL:
                return

            project = jira_create_dlg.get_project()
            sprint = jira_create_dlg.get_sprint()
            status = jira_create_dlg.get_status()
            params = {"project": project, 'sprint': sprint, 'status': status}
            try:
                conf_infos = commons.get_conf(commons.JIRA)
                list_selected_forms = []
                for my_form in selected_forms:
                    list_selected_forms.append(my_form)

                dlg = ProgressDialog("Jira", nb_formality * 10 + 2)
                dlg.Show()

                ServiceThread(commons.CREATE_JIRA_ISSUE, nb_formality,
                              list_selected_forms, conf_infos, params=params)

            except BaseException as ex:
                logging.error(str(ex))
                wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
                return

    def on_regent(self, event, user="", password=""):
        del event
        # self.lastRegentEvent = event -> en cas de login/mdp
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "Regent",
                          wx.OK | wx.ICON_ERROR)
            return
        with RegentDialog(self, "Generate a regent file") as regent_dialog:
            if regent_dialog.ShowModal() == wx.ID_CANCEL:
                return

            version = regent_dialog.version
            events = regent_dialog.list_events
            params = {"version": version, "events": events}
            try:
                conf_infos = commons.get_conf(commons.REGENT)
                if user is not "":
                    conf_infos['user'] = user
                    conf_infos['password'] = password
                list_selected_forms = []
                for my_form in selected_forms:
                    list_selected_forms.append(my_form)

                dlg = ProgressDialog("Regent", nb_formality * 10 + 2)
                dlg.Show()

                ServiceThread(commons.REGENT, nb_formality,
                              list_selected_forms, conf_infos, params=params)

            except BaseException as ex:
                logging.error(str(ex))
                wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
                return

    def on_result_progress(self, action, result):
        if action == commons.UPLOAD:
            wx.MessageBox(
                'uploaded : \n' + '%s' % '\n'.join(
                    str(my_form.get_form_name() + " -> " +
                        my_form.get_reference_id()) for my_form in result),
                "Upload", wx.OK | wx.ICON_INFORMATION)
        if action == commons.PURGE:
            wx.MessageBox(
                'Purged : \n' + '%s' % '\n'.join(
                    str(my_form.get_form_name() + " -> " +
                        my_form.get_reference_id()) for my_form in result),
                "Purge", wx.OK | wx.ICON_INFORMATION)

        if action == commons.REGENT:
            wx.MessageBox('regent generated for : \n' +
                          '%s' % '\n'.join(my_form.get_form_name()
                                           for my_form in result), "Regent",
                          wx.OK | wx.ICON_INFORMATION)
            self.load_workspace(self.workspace, mode=self.current_source)
        # if action == commons.CREATE_JIRA_ISSUE:
        #     wx.MessageBox('Jira issue generated for : \n' +
        #                   '%s' % '\n'.join(my_form.get_form_name()
        #                                    for my_form in result),
        #                   "JIRA", wx.OK | wx.ICON_INFORMATION)
        if action == commons.STRING_EX:
            wx.MessageBox('String externalized for : \n' +
                          '%s' % '\n'.join(my_form.get_form_name()
                                           for my_form in result) +
                          "\nthe file is located in the form",
                          "String externalization", wx.OK | wx.ICON_INFORMATION)
        if action == "ERROR":
            wx.MessageBox('error : ' + result, "Error", wx.OK | wx.ICON_ERROR)

    def on_sel_changed(self, event):
        # sauvegarde en memoire de la saisi utilisateur
        if self.current_form is not None and len(self.current_form) != 0:
            new_text = self.formality_editor.GetValue()
            self.current_form.set_description_xml(new_text)

        # changement de fichier
        item = event.GetItem()
        if item.GetData() is not None:
            self.make_viewer(item.GetData())

    def on_generate_package(self, event):
        del event
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        with PackageDialog(self, "Generate a package", self.use_jira) as package_dialog:

            if package_dialog.ShowModal() == wx.ID_CANCEL:
                return
            self.generate_package(selected_forms, nb_formality, package_dialog)

    def generate_package(self, selected_forms, nb_formality, package_dialog):
        app = package_dialog.app
        version = package_dialog.version
        nexus = package_dialog.nexus
        group = package_dialog.group
        list_formality_to_depublish = package_dialog.formality_to_unpublish

        generate_bl_jira = package_dialog.generate_bl_jira
        bl_jira_project = package_dialog.bl_jira_project

        # si on n'upload pas sur nexus, on sauvegarde le package en local
        package_name = app + "-" + version + ".zip"
        if nexus:
            pathname = os.path.join(self.workspace, package_name)

        # si upload sur nexus on sauvegarde le package dans le workspace
        else:
            # otherwise ask the user what new file to open
            with wx.FileDialog(self, "Save a package",
                               wildcard="zip files (*.zip)|*.zip",
                               style=wx.FD_SAVE |
                               wx.FD_OVERWRITE_PROMPT) as file_dialog:
                file_dialog.SetFilename(package_name)
                if file_dialog.ShowModal() == wx.ID_CANCEL:
                    return     # the user changed their mind

                # Proceed loading the file chosen by the user
                pathname = file_dialog.GetPath()

        list_selected_formality = []
        for my_form in selected_forms:
            my_form.set_group(group)
            list_selected_formality.append(my_form)
        try:
            package.generate_package(
                pathname, list_selected_formality, list_formality_to_depublish)
        except BaseException as error:
            wx.MessageBox("Error While generating package : \n {error}"
                          .format(error=error),
                          'Package', wx.OK | wx.ICON_ERROR)
            return

        # upload sur Nexus
        url_result = None
        message_result = None
        if nexus:
            # récupération du login/mdp/url
            infos = commons.get_conf(commons.GENERATE_PACKAGE)
            if(infos is None or infos['user'] is "" or
               infos['password'] is "" or
               infos['url'] is ""):
                wx.MessageBox(("cannot generate package : PACKAGE "
                               "section not configured correctly, "
                               "please edit with Window -> preferences"),
                              "package",
                              wx.OK | wx.ICON_ERROR)
                return
            bi_frame = wx.BusyInfo("Uploading package {package_name} on Nexus,"
                                   " please wait...".format(
                                       package_name=package_name), self)
            try:
                url_result = package.upload(pathname, infos)
            except BaseException as error:
                del bi_frame
                wx.MessageBox("Error While uploading on Nexus : \n {error}"
                              .format(error=error),
                              'Nexus', wx.OK | wx.ICON_ERROR)
                return
            else:
                del bi_frame

            if url_result is not None:
                # copie de l'url dans le clipboard
                data_obj = wx.TextDataObject()
                data_obj.SetText(url_result)
                if wx.TheClipboard.Open():
                    wx.TheClipboard.SetData(data_obj)
                    wx.TheClipboard.Close()
                message_result = ("Package generated with {nb_formality} formalities \n"
                                  " upload Nexus OK : \n {url_result}\n"
                                  " 🡆 url has been copied to clipboard 🡄").format(nb_formality=nb_formality, url_result=url_result)

        # message de generation Ok
        else:
            message_result = ("Package generated with {nb} formalities"
                              " published \n {pathname}").format(nb=nb_formality, pathname=pathname)

        if generate_bl_jira:

            try:
                params = {}
                params['version'] = version
                params['app'] = app
                params['project'] = bl_jira_project
                params['list_forms_published'] = list_selected_formality
                params['list_forms_depublished'] = list_formality_to_depublish
                params['link_nexus'] = url_result

                bl_key = jira_connection.JiraConnection().create_bl(params)
                message_result += (
                    "\n\n Jira created : {bl_key}".format(bl_key=bl_key))
            except BaseException as error:
                wx.MessageBox("Error While generating jira : \n {error}"
                              .format(error=error),
                              'Package', wx.OK | wx.ICON_ERROR)
        wx.MessageBox(message_result, "Package", wx.OK | wx.ICON_INFORMATION)

    def on_generate_selenium(self, event):
        del event
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("cannot generate selenium without formalities",
                          "package",
                          wx.OK | wx.ICON_ERROR)
            return

        for my_form in selected_forms:
            standalone_form.convert_form_to_selenium(my_form)

        wx.MessageBox("%s selenium generated" % (nb_formality), "Package",
                      wx.OK | wx.ICON_INFORMATION)

    def on_pres_flat(self, event):
        del event
        self.make_explorer(self.workspace, mode=self.MODE_EXPLORER_FLAT)

    def on_pres_hierarchical(self, event):
        del event
        self.make_explorer(
            self.workspace, mode=self.MODE_EXPLORER_HIERARCHICAL)

    def on_source_xml(self, event):
        del event
        self.current_source = commons.SOURCE_XML
        self.load_workspace(self.workspace, mode=self.current_source)

        self.selenium_item.Enable(enable=False)
        self.regent_item.Enable(enable=True)
        self.set_status_text()

    def on_source_zip(self, event):
        del event
        self.current_source = commons.SOURCE_ZIP
        self.load_workspace(self.workspace, mode=self.current_source)

        self.selenium_item.Enable(enable=True)
        self.regent_item.Enable(enable=False)
        self.set_status_text()

    def on_open_workspace(self, event):
        del event
        if self.content_not_saved:
            if wx.MessageBox("Current content has not been saved! Proceed?",
                             "Please confirm",
                             wx.ICON_QUESTION | wx.YES_NO, self) == wx.NO:
                return

        # otherwise ask the user what new file to open
        with wx.DirDialog(self, "Choose Workspace", "",
                          wx.DD_DEFAULT_STYLE |
                          wx.DD_DIR_MUST_EXIST) as dir_dialog:

            if dir_dialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            self.load_workspace(dir_dialog.GetPath(), self.current_source)

    def on_save_file(self, event):
        del event
        # Proceed loading the file chosen by the user
        my_form = self.current_form
        new_text = self.formality_editor.GetValue()

        my_form.set_description_xml(new_text)
        my_form.write()

        if my_form is not None:
            self.make_viewer(my_form)

    def on_about(self, event):
        del event
        wx.MessageBox("This is Nash IDE ! ", "About Nash IDE v"
                      + VERSION_IDE, wx.OK | wx.ICON_INFORMATION)

    def on_help(self, event):
        del event
        webbrowser.open("https://nash-tools.readthedocs.io/")

    def on_preferences(self, event):
        del event
        with PreferencesDialog(self, "Preferences") as preference_dialog:

            if preference_dialog.ShowModal() == wx.ID_CANCEL:
                return

        self.init_conf()

    def on_prompt_login(self, module):
        with LoginDialogIde(self, "Generate a package") as login_dialog:

            if login_dialog.ShowModal() == wx.ID_CANCEL:
                return

            result = login_dialog.get_result()

        if result == commons.LOGIN_CANCEL:
            raise Exception("operation aborded")
        else:
            user = result['user']
            password = result['password']
            if result['remember']:
                commons.set_conf(
                    [{'module': module, 'key': 'user', 'value': user},
                     {'module': module, 'key': 'password', 'value': password}])
            self.__action__[module](self.last_upload_event,
                                    user=user, password=password)

    def on_merge_after(self, event):
        del event
        self.on_merge(form.PLACE_END)

    def on_merge_before(self, event):
        del event
        self.on_merge(form.PLACE_BEGIN)

    def on_merge(self, position):
        selected_forms = self.get_selected_forms()
        nb_formality = len(selected_forms)

        if nb_formality == 0:
            wx.MessageBox("please select one or more formalities",
                          "merge",
                          wx.OK | wx.ICON_ERROR)
            return
        # otherwise ask the user what new file to open
        with wx.FileDialog(self, "choose a form to merge",
                           wildcard="ZIP and XML files "
                           "(*.zip;*.xml)|*.zip;*.xml",
                           style=wx.FD_OPEN) as file_dialog:
            if file_dialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind

            # Proceed loading the file chosen by the user
            pathname = file_dialog.GetPath()
        try:
            if pathname.endswith(".zip"):
                form_to_merge = zip_form.ZipForm(pathname)
            elif pathname.endswith(".xml"):
                form_to_merge = xml_form.XmlForm(pathname)

            for my_form in selected_forms:
                my_form.merge_form(form_to_merge, place=position)
                my_form.write(full_save=True)
            wx.MessageBox('form %s merged.' % form_to_merge.get_form_name(),
                          "merge", wx.OK | wx.ICON_INFORMATION)

            self.load_workspace(self.workspace, mode=self.current_source)
        except BaseException as ex:
            commons.logging.error(str(ex))
            wx.MessageBox(str(ex), "error", wx.ICON_ERROR)
            return

    def on_search_cancel(self, _):
        print("on_search_cancel" + self.search_field.GetValue())
        print(self.search_field.IsCancelButtonVisible())

    def on_search(self, _):
        print("on_search" + self.search_field.GetValue())
        print(self.search_field.IsCancelButtonVisible())

    def check_new_version(self):
        new_version = commons.check_last_version(VERSION_IDE)
        quit = False
        if new_version:
            description = new_version['description']
            url = new_version['url']
            dialog = wx.MessageDialog(self, '{description}\n\nVoulez vous télécharger cette nouvelle version ?'.format(description=description),
                                      "Nouvelle version disponible !", wx.YES_NO | wx.ICON_INFORMATION)

            if dialog.ShowModal() == wx.ID_YES:
                webbrowser.open(url)
                quit = True
            dialog.Destroy()
        return quit


if __name__ == '__main__':
    if commons.is_frozen():
        __level__ = logging.DEBUG
    else:
        __level__ = logging.DEBUG
    commons.set_logging_system(level=__level__)
    __app_nash__ = wx.App()
    __frm_nash__ = NashIdeFrame(None, title='Nash IDE v' +
                                VERSION_IDE, size=wx.Size(1000, 600))
    __frm_nash__.Show()
    __frm_nash__.Centre()
    __app_nash__.MainLoop()
