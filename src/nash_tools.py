﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import os
import sys
import ctypes
import logging
import argparse
import traceback
import webbrowser

from nash.services import standalone_form
from nash.entities import xml_form, zip_form
from nash.services import commons


def is_frozen():
    return getattr(sys, 'frozen', False)

###############################################################################
# Find the filename of this file (depend on the frozen or not)
###############################################################################


def get_this_filename():
    result = ""
    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__
    return result

###############################################################################


def message_box(text, title):
    ctypes.windll.user32.MessageBoxW(0, text, title, 0)


###############################################################################
# Parameters
###############################################################################
__logo__ = r"""
  _   _           _           _              _
 | \ | | __ _ ___| |__       | |_ ___   ___ | |___
 |  \| |/ _` / __| '_ \ _____| __/ _ \ / _ \| / __|
 | |\  | (_| \__ \ | | |_____| || (_) | (_) | \__ \\
 |_| \_|\__,_|___/_| |_|      \__\___/ \___/|_|___/
"""

###############################################################################
# Define the parsing of arguments of the command line
###############################################################################
def get_parser_for_command_line():
    description_arg = """This program take a list of standalone forms and """ \
        """convert them to a selenese test case script or upload""" \
        """ them to a forge."""

    parser = argparse.ArgumentParser(description=description_arg)
    parser.add_argument('--windows', action='store', dest='windows',
                        choices=['yes', 'no'], default='yes',
                        help='Define if we need all popups windows.')
    parser.add_argument('--verbose', '-v',
                        action='store_true', dest='verbose',
                        help='Put the logging system on the console for info.')
    parser.add_argument('--upload', '-u', action='store_true',
                        dest='upload', help='upload a form')
    parser.add_argument('--build', '-b', action='store_true',
                        dest='build', help='build a form')
    parser.add_argument('--browser', '-B', action='store_true',
                        dest='browser', help='open a browser if upload')
    parser.add_argument('--selenium', '-s', action='store_true',
                        dest='selenium', help='generate a selenium test')
    parser.add_argument('--translate', '-t', action='store_true',
                        dest='translate', help='prepare a translate file')
    parser.add_argument('filenames', metavar='filename',
                        nargs='+', help='list of filenames.')

    return parser

###############################################################################
# Main script
###############################################################################
def main():

    try:
        parser = get_parser_for_command_line()
        args = parser.parse_args()

        logging.info('+')
        logging.info(
            '-------------------------------------------------------->>')
        logging.info('Started %s', get_this_filename())
        logging.info('The Python version is %s', '.'.join(
            str(x) for x in sys.version_info[:-2]))
        logging.info(__logo__)

        args.windows_bool = (args.windows == "yes")
        logging.info("verbose=%s", args.verbose)
        logging.info("windows=%s", args.windows)
        logging.info("browser=%s", args.browser)
        logging.info("filenames=%s", repr(args.filenames))

        count = 1
        max_file = len(args.filenames)

        error_msg = []
        form = None
        if args.translate:
            for filename in args.filenames:
                local_filename = os.path.abspath(filename)
                logging.info(">>> Build : Working on %03d / %03d : %s",
                             count, max_file, local_filename)
                count = count + 1
                try:
                    if local_filename.endswith('.zip'):
                        form = zip_form.ZipForm(local_filename, load_form=True)
                    else:
                        form = xml_form.XmlForm(local_filename, load_form=True)

                    standalone_form.generate_prepare_translate(form)
                    form.write(full_save=True)
                except BaseException as ex:
                    var = traceback.format_exc()
                    logging.error('Unknown error : \n%s', var)
                    # accumulate all error
                    error_msg.append((local_filename, str(ex)))
        if args.upload:
            uploaded = []

            for filename in args.filenames:
                local_filename = os.path.abspath(filename)
                logging.info(">>> Upload : Working on %03d / %03d : %s",
                             count, max_file, local_filename)
                count = count + 1
                try:
                    infos = commons.get_conf(commons.UPLOAD)
                    if local_filename.endswith('.zip'):
                        form = zip_form.ZipForm(local_filename)
                    else:
                        form = xml_form.XmlForm(local_filename)

                    url_form = standalone_form.upload_standalone_form(
                        form, infos)
                    uploaded.append(
                        {'filename': os.path.split(form.get_filename())[1],
                         'reference-id': form.get_reference_id(),
                         'url': url_form})
                    logging.info('%s', form)
                except BaseException as ex:
                    var = traceback.format_exc()
                    logging.error('Unknown error : \n%s', var)
                    # accumulate all error
                    error_msg.append((local_filename, str(ex)))

            if len(uploaded) > 0:
                if args.browser:
                    for form_uploaded in uploaded:
                        webbrowser.open(form_uploaded['url'])
                else:
                    message_box(
                        text='uploaded : ' + '%s' % ', '
                        .join(str(form_uploaded['filename'] + " -> "
                                  + form_uploaded['reference-id'])
                              for form_uploaded in uploaded), title='Upload')
        if args.build:
            for filename in args.filenames:
                local_filename = os.path.abspath(filename)
                logging.info(">>> Build : Working on %03d / %03d : %s",
                             count, max_file, local_filename)
                count = count + 1
                try:
                    xml_form.XmlForm(local_filename).build()
                except BaseException as ex:
                    var = traceback.format_exc()
                    logging.error('Unknown error : \n%s', var)
                    # accumulate all error
                    error_msg.append((local_filename, str(ex)))
        if args.selenium:
            for filename in args.filenames:
                local_filename = os.path.abspath(filename)
                logging.info(">>> Selenium : Working on %03d / %03d : %s",
                             count, max_file, local_filename)
                count = count + 1
                try:
                    form = zip_form.ZipForm(local_filename)
                    standalone_form.convert_form_to_selenium(
                        form, os.path.splitext(local_filename)[1])

                except BaseException as ex:
                    var = traceback.format_exc()
                    logging.error('Unknown error :\n%s', var)
                    # accumulate all error
                    error_msg.append((local_filename, str(ex)))

        if len(error_msg) > 0:
            msg = "The following error(s) appends during "\
                "all the process : \r\n\r\n"
            for error in error_msg:
                msg = msg + "%s : \n\n '%s'\r\n" % (error[0], error[1])
            msg = msg + "\n\n No others errors appends on others files.\r\n"
            raise Exception(msg)

    except argparse.ArgumentError as errmsg:
        logging.error(str(errmsg))
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=parser.format_usage(), title='Usage')

    except SystemExit:
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=parser.format_help(), title='Help')

    except BaseException as ex:
        logging.error(str(ex))
        if ('args' in locals()) and (args.windows_bool):
            message_box(text=str(ex), title='Usage')

    logging.info('Finished')
    logging.info('<<--------------------------------------------------------')


###############################################################################
# Call main if the script is main
###############################################################################
if __name__ == '__main__':
    commons.set_logging_system()
    main()
