﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import re
import sys
import codecs
import logging
import os.path
import requests
import tempfile
import configparser

from urllib.parse import urlparse
# import lxml.etree as etree


LOGIN_CANCEL = "CANCEL"

RESSOURCES_PATH = "ressources"
RESSOURCES_JIRA_PATH = os.path.join(RESSOURCES_PATH, "jira")
RESSOURCES_REGENT_PATH = os.path.join(RESSOURCES_PATH, "regent")
RESSOURCES_SELENIUM_PATH = os.path.join(RESSOURCES_PATH, "selenium")

# module & configuration
JIRA = "JIRA"
GENERATE_PACKAGE = "PACKAGE"
UPLOAD = "UPLOAD"
PURGE = "PURGE"
STRING_EX = "STRING_EX"
REGENT = "REGENT"
DEFAULT = "DEFAULT"

CREATE_JIRA_ISSUE = "CREATE_JIRA_ISSUE"

MODULES = [JIRA, GENERATE_PACKAGE, UPLOAD, REGENT]

SOURCE_ZIP = 10
SOURCE_XML = 20

def is_frozen():
    return getattr(sys, 'frozen', False)

###############################################################################
# Find the filename of this file (depend on the frozen or not)
###############################################################################
def get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Test a folder
# Test if the folder exist.
#
# @exception RuntimeError if the name is a file or not a folder
#
# @param folder the folder name
# @return the folder normalized.
###############################################################################
def check_folder(folder):
    if os.path.isfile(folder):
        logging.error('%s can not be a folder (it is a file)', folder)
        raise RuntimeError('%s can not be a folder (it is a file)' % folder)

    if not os.path.isdir(folder):
        logging.error('%s is not a folder', folder)
        raise RuntimeError('%s is not a folder' % folder)

    return set_correct_path(folder)

###############################################################################
# Retrive the correct complet path
###############################################################################
def set_correct_path(the_path):
    return os.path.abspath(the_path)

###############################################################################
# test if this is a file and correct the path
###############################################################################
def test_is_file_and_correct_path(filename):
    filename = set_correct_path(filename)

    if not os.path.isfile(filename):
        logging.error('"%s" is not a file', filename)
        raise Exception('"%s" is not a file' % (filename))

    return filename

###############################################################################
# test if this is a file and correct the path
###############################################################################
def test_is_directory(directory):
    directory = set_correct_path(directory)

    if not os.path.isdir(directory):
        logging.error('"%s" is not a directory', (directory))
        raise Exception('"%s" is not a directory' % (directory))

    return directory

###############################################################################
# Test a folder
###############################################################################
def create_path(path):
    paths_to_create = []
    while not os.path.lexists(path):
        paths_to_create.insert(0, path)
        head, tail = os.path.split(path)
        if len(tail.strip()) == 0:  # Just incase path ends with a / or /
            path = head
            head, tail = os.path.split(path)
        path = head

    for path in paths_to_create:
        os.mkdir(path)


###############################################################################
# Logging system
###############################################################################
def set_logging_system(level=logging.INFO):
    log_filename = os.path.abspath(os.path.join(os.path.split(
        os.path.realpath(get_this_filename()))[0], '../../', 'nash-tools.log'))

    if is_frozen():
        log_filename = os.path.abspath(os.path.join(
            tempfile.gettempdir(), 'nash-tools.log'))

    logging.basicConfig(filename=log_filename, level=logging.INFO,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(level)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Get the content of a file. This function delete the BOM.
#
# @param filename the file name
# @param encoding the encoding of the file
# @return the content
###############################################################################
def get_file_content(filename, encoding="utf-8"):
    logging.debug('Get content of the filename %s', filename)
    filename = test_is_file_and_correct_path(filename)

    try:
        input_file = codecs.open(filename, mode="r", encoding=encoding)
        type_bytes = False
        content = input_file.read()
    except UnicodeDecodeError:
        input_file = codecs.open(filename, mode="rb")
        type_bytes = True
        content = input_file.read()

    input_file.close()

    if not type_bytes and content.startswith(u'\ufeff'):
        content = content[1:]

    return content

###############################################################################
# check the configuration and create the missing properties
#
# @param config the configuration
###############################################################################
def check_conf(config):
    # if filenotfound

    # add missing conf
    for module in MODULES:
        if module not in config:
            config[module] = {}

        if 'user' not in config[module]:
            config[module]['user'] = ""

        if 'password' not in config[module]:
            config[module]['password'] = ""

        if 'url' not in config[module]:
            config[module]['url'] = ""

    # default
    if DEFAULT not in config:
        config[DEFAULT] = {}

    if 'use_jira' not in config[JIRA]:
        config[JIRA]['use_jira'] = str(False)

    if 'use_forms_uid' not in config[UPLOAD]:
        config[UPLOAD]['use_forms_uid'] = str(False)

    if 'author' not in config[UPLOAD]:
        config[UPLOAD]['author'] = ""

    if 'group' not in config[UPLOAD]:
        config[UPLOAD]['group'] = ""

    if 'publish' not in config[UPLOAD]:
        config[UPLOAD]['publish'] = str(False)

    if 'default_conf' in config[UPLOAD]:
        default_section_name = config[UPLOAD]['default_conf']

        if default_section_name not in config:
            config[default_section_name] = {}
        if 'use_forms_uid' not in config[default_section_name]:
            config[default_section_name]['use_forms_uid'] = str(False)

        if 'author' not in config[default_section_name]:
            config[default_section_name]['author'] = ""

        if 'group' not in config[default_section_name]:
            config[default_section_name]['group'] = ""

        if 'publish' not in config[default_section_name]:
            config[default_section_name]['publish'] = str(False)


###############################################################################
# get the configuration
#
# @param module the section to return (if None, then return the
#   obect ConfigParser)
# @return the configuration
###############################################################################
def get_conf(module=None):
    config = configparser.ConfigParser()
    if not is_frozen():
        path = os.path.join('confdev', 'nash-tools.conf')
    else:
        local_app_data_nash = os.path.join(
            os.getenv('LOCALAPPDATA'), "nash-tools")
        path = os.path.join(local_app_data_nash, "nash-tools.conf")

    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, "w"):
            pass

    config.read(path)
    logging.info("configuration file : %s", os.path.abspath(path))

    check_conf(config)

    try:
        if module is not None:
            return config[module]
        else:
            return config
    except KeyError as ex:
        raise Exception('"%s" error configuration file : %s' % (path, ex))
    return None

###############################################################################
# set the configuration
#
# @param config the configuration
# @return the content
###############################################################################
def set_conf(config_list=None, list_to_remove=None):
    config = configparser.ConfigParser()

    if not is_frozen():
        path = os.path.join('confdev', 'nash-tools.conf')
    else:
        local_app_data_nash = os.path.join(
            os.getenv('LOCALAPPDATA'), "nash-tools")
        create_path(local_app_data_nash)
        path = os.path.join(local_app_data_nash, "nash-tools.conf")

    logging.info("maj du fichier de conf : %s", path)

    # check_conf(config)

    config.read(path)

    if list_to_remove is not None:
        for conf in list_to_remove:
            config.remove_section(conf)

    if config_list is not None:
        for conf in config_list:
            if not config.has_section(conf['module']):
                config[conf['module']] = {}

            config[conf['module']][conf['key']] = conf['value']

    with open(path, 'w') as configfile:
        config.write(configfile)

###############################################################################
# Get boolean according to the string
#
# @param value the string to test
# @return the boolean value
###############################################################################
def str2bool(value):
    return value.lower() in ("yes", "true", "t", "1")


# def prettify_xml(xml_string):
#     my_xml = etree.XML(xml_string.encode('utf-8'))
#     return etree.tostring(my_xml, pretty_print=True).decode("utf-8")


###############################################################################
# Get boolean according to the string
#
# @param current_version the current version of this program
# @return the url of the new version
###############################################################################
def check_last_version(current_version):
    url_gitlab = "gitlab.com/guichet-entreprises.fr/tools/nash-tools"
    request_git = requests.get(
        "https://gitlab.com/api/v4/projects/13777201/releases")

    if request_git.status_code != 200:
        return None

    last_release = request_git.json()[0]

    if current_version != last_release['name']:
        asset_url = None

        # first we're looking in the assets
        try:
            links = last_release['assets']['links']
            for link in links:
                if link['name'] == "download":
                    asset_url = link['url']
        except KeyError as ex:
            logging.error("check_new version error : %s not exists", ex)

        # if we didn't found the link we look in the release description
        if not asset_url:
            regex = r"\[setup_nash-tools-.*\]\((.*)\)"
            matches = re.search(
                regex, last_release['description'], re.MULTILINE)
            if matches:
                asset_url = matches.group(1)

        if not asset_url:
            return None

        url = urlparse(asset_url)
        if url.netloc == "":
            url = url._replace(netloc=url_gitlab)
            url = url._replace(scheme="https")
        return {"description": last_release['description'],
                "url": url.geturl()}

    return None
