﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################
import os
import logging

from bs4 import BeautifulSoup

def get_prepare_translation(form):
    logging.debug("parsing the file to retrive strings")

    text = ""
    form.load()

    for the_file in form.get_files().keys():
        if the_file.endswith(".xml"):
            parser = BeautifulSoup(form.get_files()[the_file], "xml")
            infos = [os.path.basename(the_file)]
            text += get_all_data(parser, infos)

    return text

def get_all_data(parser, infos=[]):
    text_to_translate = ""

    list_token = ["data", "text", "value", "help", "group",
                  "description", "warning", "title"]

    for token in list_token:
        for text in parser.find_all(token):
            if 'id' in text.attrs.keys():
                infos.append(token + " id = " + text['id'])
            else:
                infos.append(token)

            infos.append(xpath_soup(text))

            if 'label' in text.attrs.keys():
                text_to_translate += get_translate_template(
                    text['label'], infos)
            if len(text.findChildren()) == 0:
                text_to_translate += get_translate_template(
                    text.text, infos)

            infos.pop()
            infos.pop()

    return text_to_translate

def get_translate_template(token, infos=[]):
    text = "#TRANSLATORS: "
    text += " | ".join(infos)

    return text + "\n_(\"" + token + "\")\n\n"


def xpath_soup(element):
    # type: (typing.Union[bs4.element.Tag, bs4.element.NavigableString]) -> str
    """
    Generate xpath from BeautifulSoup4 element.
    :param element: BeautifulSoup4 element.
    :type element: bs4.element.Tag or bs4.element.NavigableString
    :return: xpath as string
    :rtype: str
    Usage
    -----
    >>> import bs4
    >>> html = (
    ...     '<html><head><title>title</title></head>'
    ...     '<body><p>p <i>1</i></p><p>p <i>2</i></p></body></html>'
    ...     )
    >>> soup = bs4.BeautifulSoup(html, 'html.parser')
    >>> xpath_soup(soup.html.body.p.i)
    '/html/body/p[1]/i'
    >>> import bs4
    >>> xml = '<doc><elm/><elm/></doc>'
    >>> soup = bs4.BeautifulSoup(xml, 'lxml-xml')
    >>> xpath_soup(soup.doc.elm.next_sibling)
    '/doc/elm[2]'
    """
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
            )
        )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)
