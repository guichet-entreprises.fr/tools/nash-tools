﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import os
import codecs
import logging
import threading
import requests

import wx
from wx.lib.pubsub import pub as Publisher

from nash.gui import login_dialog
from nash.services import commons


def get_regent(version, events, conf_infos):

    api_url = conf_infos['url']
    user = conf_infos['user']
    password = conf_infos['password']

    params = {'regentVersion': version, 'listTypeEvenement': events}

    tryagain = True
    while tryagain:

        # appel au WS regent
        req = requests.get(api_url, params=params, timeout=10)

        if req.status_code == 401:
            # thread principal (mode console)
            if isinstance(threading.current_thread(), threading._MainThread):
                result = login_dialog.launch_login_dialog(
                    title="nash-tools REGENT")
            # thread secondaire (mode IHM)
            else:
                wx.CallAfter(Publisher.sendMessage,
                             "promptLogin", module=commons.REGENT)
                raise Exception("login needed")

            if result == commons.LOGIN_CANCEL:
                raise Exception("operation aborded")
            else:
                user = result['user']
                password = result['password']
                if result['remember']:
                    commons.set_conf(
                        [{'module': commons.REGENT,
                          'key': 'user',
                          'value': user},
                         {'module': commons.REGENT,
                          'key': 'password',
                          'value': password}])

        elif req.status_code != 200:
            error_content = req.content.decode('UTF-8')
            raise Exception("error while calling regent, code : {code}"
                            "\n{content}".format(
                                code=req.status_code, content=error_content))

        else:
            tryagain = False
    logging.info('call regent %s', req.url)

    return req.text


def add_step_xml_regent(form, text_regent):
    destination_dir = os.path.join(
        os.path.split(form.get_filename())[0], "regent")
    regent_filename = os.path.join(destination_dir, "preprocess.js")
    commons.create_path(destination_dir)
    writer = codecs.open(regent_filename, encoding='utf-8', mode='wb')

    writer.write(text_regent)
    writer.close()

    # ajout du preprocess.xml
    preprocess_xml_filename = os.path.join(destination_dir, "preprocess.xml")

    path_preprocess_ressources = os.path.join(commons.RESSOURCES_REGENT_PATH,
                                              "preprocess.xml")
    content_preprocess_xml = commons.get_file_content(
        path_preprocess_ressources)

    writer = codecs.open(preprocess_xml_filename, encoding='utf-8', mode='wb')

    writer.write(content_preprocess_xml)
    writer.close()

    # maj du fichier description.xml
    xml = form.get_description_xml()
    new_tag = xml.new_tag('step', data="regent/data-generated.xml",
                          preprocess="regent/preprocess.xml",
                          user="ge",
                          id='regent')
    xml.description.steps.append(new_tag)
    xml.description.steps.append("\n")

    form.set_description_xml(str(xml))
    form.write(build=True)
