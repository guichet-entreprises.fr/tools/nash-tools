﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx
import os
import jinja2
import os.path
import requests

from jira import JIRA
from urllib.parse import urlparse
from nash.services import commons
from nash.services.exceptions import BusinessException

ID_STATUS_DONE = '61'
ID_RESULUTION_DONE = '10000'


class JiraConnection():

    __url = ""
    __user = ""
    __password = ""
    __jira = None
    __reconnect = True
    __id_status_done = None
    __id_resolution_done = None

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, '_JiraConnection'):
            cls._JiraConnection = super(
                JiraConnection, cls).__new__(cls, *args, **kwargs)
        return cls._JiraConnection

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        if(not url.startswith("http") and not url.startswith("//")):
            url = "//" + url
        url_object = urlparse(url)
        if(url_object.scheme == "http" or url_object.scheme == ""):
            url_object = url_object._replace(scheme="https")
        self.__url = url_object.geturl()
        self.__reconnect = True

    @property
    def user(self):
        return self.__user

    @user.setter
    def user(self, user):
        self.__user = user
        self.__reconnect = True

    @property
    def password(self):
        return self.__password

    @password.setter
    def password(self, password):
        self.__password = password
        self.__reconnect = True

    def __str__(self):
        result = ""
        result += "Jira connection :\n"
        result += "url :%s\n" % (self.url)
        result += "user :%s\n" % (self.user)
        return result

    def get_connector(self):
        if self.__reconnect:
            self.__jira = JIRA(self.url, basic_auth=(
                self.user, self.password), max_retries=0, validate=True)
            self.__reconnect = False
        return self.__jira

    def search_one_issues(self, reference):
        request = """ Référence ~ "%s" """ % (reference)
        try:
            issues = self.get_connector().search_issues(
                request, fields='summary,status,assignee')
        except jira.exceptions.JIRAError as errmsg:
            logging.error(str(errmsg))
            message_box(text=parser.format_usage(), title='Jira down')

        if len(issues) == 0:
            raise BusinessException(
                'No jira issue found %s' % reference)
        return issues[0]

    def add_comment(self, issue, comment):
        self.get_connector().add_comment(issue, comment)

    def set_fields(self, issue, key, value):
        issue.update(fields={key: value})

    def set_status_done(self, issue):
        # if self.__id_status_done is None:
        # transitions = self.get_connector().transitions(issue)
        # (item for item in transitions if item["name"] == "Done").next()

        # if self.__id_resolution_done is None:
        #    resolutions = self.get_connector().resolutions()
        # print(transitions)
        # print(resolutions)

        self.get_connector().transition_issue(
            issue, ID_STATUS_DONE,
            resolution={'id': ID_RESULUTION_DONE})
        self.add_comment(issue, "nashtools : Update status to DONE")

    def create_bl(self, params):
        path_template_bl = os.path.join(commons.RESSOURCES_JIRA_PATH, "bl.j2")

        content = jinja2.Template(commons.get_file_content(path_template_bl))
        # id '10101' => Delivery Bill
        new_issue = self.get_connector().create_issue(
            project=params['project'],
            summary=f"[{params['version']}][BL] {params['app']}",
            description=content.render(params),
            issuetype={'id': '10101'})
        return new_issue.key

    def get_jira_project(self):
        results = []
        projects = self.get_connector().projects()

        for project in projects:
            # image = project.raw['avatarUrls']['16x16']
            # response = requests.get(project.raw['avatarUrls']['16x16'],
            #                         auth=(self.user, self.password))
            # image = None
            # try:
            #     image = wx.Image(response.content,
            #                      type=wx.BITMAP_TYPE_PNG).ConvertToBitmap()
            # except ValueError as ex:
            image = wx.Bitmap(wx.Image(os.path.join(
                'img', 'jira_project.png'), wx.BITMAP_TYPE_ANY))
            results.append({'key': project.key, "image": image})
        return results

# if __name__ == '__main__':

#     j_con_2 = JiraConnection()
#     j_con_2.url = "https://tools.projet-ge.fr/jira"
#     j_con_2.user = "arnaud.boidard"
#     j_con_2.password = "Gipge001!"

#     fiche_1 = j_con_2.search_one_issues("nash-tools/deploy/JQPA")
#     fiche_2 = j_con_2.search_one_issues("nash-tools/selenium/testtype")

#     list_issues = []
#     list_issues.append(fiche_1)
#     list_issues.append(fiche_2)

#     j_con_2.create_bl(list_issues)
