﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import os
import re
import ast
import zlib
import json
import shutil
import urllib
import zipfile
import logging
import threading
import requests
from requests.auth import HTTPBasicAuth
import wx
from wx.lib.pubsub import pub as Publisher

from nash.gui import login_dialog
from nash.services import commons
from nash.services import regent
from nash.services import translate
from nash.entities import zip_form, xml_form

API_CONTEXT = '/api/v1/Specification'
CONTEXT = ''

###############################################################################
# get a regent file by calling a webservice
#
# @param filename the standalone filename
# @param conf_infos the user configuration
# @param version the version of the regent
# @param events the events
# @return a regent file
###############################################################################
def generate_regent(form, conf_infos, version, events):
    logging.info("generating Regent %s %s %s",
                 version, events, form.get_form_name())

    text_regent = regent.get_regent(version, events, conf_infos)

    regent.add_step_xml_regent(form, text_regent)

###############################################################################
# get a prepare translate file
#
# @param filename the standalone filename
# @return a prepare translate file
###############################################################################
def generate_prepare_translate(form):
    the_text = translate.get_prepare_translation(form)
    filename = os.path.join(os.path.split(form.get_filename())[0],
                            os.path.basename(
        "prepare_translation.txt"))
    form.add_content(filename, the_text)

###############################################################################
# This function take a file, load the content and create the selenese file.
#
# @param filename The name and path of the file to work with. This file is
# supposed to be a markdown file.
# @param filename_ext This parameter the .zip extension for the filename.
###############################################################################
def convert_form_to_selenium(form, filename_ext=".zip"):
    filename = form.get_zip_filename()
    logging.info('Convert Satndalone form -> selenese %s', filename)
    filename = commons.test_is_file_and_correct_path(filename)
    if os.path.splitext(filename)[1] != filename_ext:
        raise Exception(
            'The extension of the file %s is %s and not %s as'
            ' expected.' % (filename, os.path.splitext(filename)[1],
                            filename_ext))

    test_cases = form.convert_to_selenese()

    path_destination = os.path.splitext(filename)[0] + "_selenium"
    commons.create_path(path_destination)
    step = 1
    for test in test_cases:
        test.writeFile(
            os.path.join(path_destination,
                         "step%s_%s.html" % (step, test.getName())))
        step += 1

    if commons.is_frozen():
        abspath = os.path.abspath(os.path.join(
            os.path.split(__file__)[0], "..", "..", "..",
            commons.RESSOURCES_SELENIUM_PATH))
    else:
        abspath = os.path.abspath(commons.RESSOURCES_SELENIUM_PATH)

    shutil.copyfile(os.path.join(abspath, "__suite__.html"),
                    os.path.join(path_destination, "__suite__.html"))
    shutil.copyfile(os.path.join(abspath, "dev_case.html"),
                    os.path.join(path_destination, "dev_case.html"))
    shutil.copyfile(os.path.join(abspath, "doc.html"),
                    os.path.join(path_destination, "doc.html"))

###############################################################################
# upload one standalone_form
#
# @param filename the standalone filename
# @param conf_infos the user configuration
# @return a list of descriptions of the form uploaded
# 	{'filename' : filename, 'reference-id' : rfid }
###############################################################################
def upload_standalone_form(form, conf_infos):

    path_zip = form.get_zip_filename(build=True)
    code_zip = form.get_form_uid()

    logging.info('uploading %s', path_zip)

    filehandle = open(path_zip, 'rb')
    file = filehandle.read()

    url = conf_infos['url']
    user = conf_infos['user']
    password = conf_infos['password']
    use_forms_uid = commons.str2bool(conf_infos['use_forms_uid'])
    group = conf_infos['group']
    author = conf_infos['author']
    publish = conf_infos['publish']

    if(use_forms_uid and code_zip is not None):
        url = url + "/" if not url.endswith("/") else url
        url = url + "code/" + code_zip

    params = {}
    params['published'] = publish
    if author:
        params['author'] = author
    else:
        params['author'] = '2017-05-USR-FGS-42'

    if group:
        params['group'] = group

    params_encoded = urllib.parse.urlencode(params)
    headers = {"Content-Type": "application/zip"}

    try_again = True

    while try_again:
        response = requests.post(url + '?' + params_encoded, headers=headers,
                                 auth=HTTPBasicAuth(user, password), data=file)

        if response.status_code == 401:
            # thread principal (mode console)
            if threading.current_thread() is threading.main_thread():
                result = login_dialog.launch_login_dialog(
                    title="nash-tools UPLOAD")
            # thread secondaire (mode IHM)
            else:
                wx.CallAfter(Publisher.sendMessage,
                             "promptLogin", module=commons.UPLOAD)
                raise Exception("login needed")

            if result == commons.LOGIN_CANCEL:
                raise Exception("operation aborded")
            else:
                user = result['user']
                password = result['password']
                if result['remember']:
                    commons.set_conf([{'module': commons.UPLOAD,
                                       'key': 'user',
                                       'value': user},
                                      {'module': commons.UPLOAD,
                                       'key': 'password',
                                       'value': password}])

        elif response.status_code != 201:
            error_content = response.content.decode('UTF-8')
            raise Exception(
                "error while uploading,"
                " code : {code}\n{content}".format(
                    code=response.status_code,
                    content=error_content))
            # try_again = False

        else:
            # mise en forme de la réponse pour la charger dans un dict
            response_str = response.text.replace("null", "None")
            response_str = response_str.replace("false", "False")
            response_str = response_str.replace("true", "True")
            new_code = ast.literal_eval(response_str)['code']

            try_again = False
            if use_forms_uid and code_zip is None:
                # on écrit le nouveau formuid dans la formalité
                form.set_form_uid(new_code)
                # écriture et rebuild pour mettre à jour le formuid dans
                # le zip également si on upload en mode dézipé
                form.write(build=True)

            # construction de l'url d'appel de la formalité
            regex = r".*\/\/.*?\/"
            url_form = re.findall(regex, url)
            return url_form[0].replace("-ws", "") + "form/" + new_code + "/use"

###############################################################################
# upload a list of standalone_forms
#
# @param listforms the list of the forms to upload
# @param conf_infos the user configuration
# @return a list of descriptions of the forms uploaded
# 	{'filename' : filename, 'reference-id' : rfid }
###############################################################################
def upload_standalone_forms(listforms, conf_infos):
    result = []
    for form in listforms:
        upload_standalone_form(form, conf_infos)
        result.append({'filename': form.get_form_name(),
                       'reference-id': form.get_reference_id()})

    return result

###############################################################################
# return the standalone_form from a .zip file
#
# @param file the form to load
# @return the object form
###############################################################################
def get_form_zip(file):
    try:
        form = zip_form.ZipForm(file, load_form=False)
    except zipfile.BadZipFile:
        logging.error(
            "%s can not be a formality (File is not a zip file)", file)
        return None
    except zlib.error:
        logging.error("%s can not be a formality (Error -3 while decompressing"
                      " data: invalid stored block lengths)", file)
        return None

    return form if not form.inconsistent else None

###############################################################################
# return the standalone_form from a description.xml file
#
# @param description_xml the form to load
# @param build if true, a .zip is produced
# @return the object form
###############################################################################
def get_form_xml(description_xml):
    if not os.path.isfile(description_xml):
        logging.error("%s can not be a formality (it is not a file)",
                      description_xml)
        return None
    if not description_xml.endswith('description.xml'):
        logging.error("%s can not be a formality (it is not a "
                      "description.xml file)", description_xml)
        return None
    try:
        form = xml_form.XmlForm(description_xml, load_form=False)
    except zipfile.BadZipFile:
        logging.error("%s can not be a formality "
                      "(File is not a zip file)", description_xml)
        return None
    except zlib.error:
        logging.error("%s can not be a formality (Error -3 while"
                      " decompressing data: invalid stored block"
                      " lengths)", description_xml)
        return None
    try:
        form.get_description()
    except TypeError:
        logging.error("% s can not be a formality(it does not contain"
                      " description.xml)", description_xml)
        return None
    except AttributeError:
        logging.error("%s error in description.xml (it does not contain "
                      "the description tag)", description_xml)
        return None
    return form

###############################################################################
# return the the list of forms in the directory
#
# @param dir_name the name of the directory
# @return the list of the files
###############################################################################
def get_formality_from_dir(dir_name, src=commons.SOURCE_XML):
    logging.debug('get content of the directory %s', dir_name)

    commons.check_folder(dir_name)
    result = []
    for root, _, files in os.walk(dir_name):
        for file in files:
            form = None
            if src == commons.SOURCE_XML:
                if file.endswith("description.xml"):
                    form = get_form_xml(os.path.join(root, file))
            else:
                if file.endswith(".zip"):
                    form = get_form_zip(os.path.join(root, file))
            if form is not None:
                result.append(form)

    return result

###############################################################################
# return the the filename of the description.xml of the .zip forms
#
# @param filename the filename of the .zip
# @return the path of the descrption.xml
###############################################################################
def get_path_zip_to_description_xml(filename):

    if not filename.endswith('.zip'):
        raise Exception("this is not a zip file : %s" % filename)

    path = os.path.abspath(filename)
    path.split()

def purge_remote_server(form, conf_infos):

    code = form.get_reference_id()

    logging.info('purge %s', code)

    url = conf_infos['url']
    user = conf_infos['user']
    password = conf_infos['password']

    msg = "purge {code} on {url}\n" \
        "searching forms...".format(code=code, url=url)
    Publisher.sendMessage("update_progress", msg=msg, increment=0)

    params = {}
    params['filters'] = 'reference:' + code
    params['maxResults'] = 1000

    params_encoded = urllib.parse.urlencode(params)

    try_again = True

    nb_forms_purged = 0

    while try_again:
        response = requests.get(url + '?' + params_encoded,
                                auth=HTTPBasicAuth(user, password))

        if response.status_code == 401:
            # thread principal (mode console)
            if threading.current_thread() is threading.main_thread():
                result = login_dialog.launch_login_dialog(
                    title="nash-tools UPLOAD")
            # thread secondaire (mode IHM)
            else:
                wx.CallAfter(Publisher.sendMessage,
                             "promptLogin", module=commons.UPLOAD)
                raise Exception("login needed")

            if result == commons.LOGIN_CANCEL:
                raise Exception("operation aborded")
            else:
                user = result['user']
                password = result['password']
                if result['remember']:
                    commons.set_conf([{'module': commons.UPLOAD,
                                       'key': 'user',
                                       'value': user},
                                      {'module': commons.UPLOAD,
                                       'key': 'password',
                                       'value': password}])

        else:
            try_again = False
            list_to_purge = json.loads(response.text)['content']

            for form_to_purge in list_to_purge:
                msg = "purge {code} on {url}\n" \
                    "deleting {uid}...".format(
                        code=code, url=url, uid=form_to_purge['code'])
                Publisher.sendMessage("update_progress", msg=msg, increment=0)
                if form_to_purge['tagged'] is False:
                    print(form_to_purge['code'] + form_to_purge['reference'])
                    response = requests.delete(url + "/code/" + form_to_purge['code'],
                                               auth=HTTPBasicAuth(user, password))

                nb_forms_purged = nb_forms_purged + 1
    return nb_forms_purged
