﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import os
import re
import sys
import logging

from bs4 import BeautifulSoup

import krypton

from nash.services import commons

###############################################################################
# Retrive the correct complet path
###############################################################################
def set_correct_path(directory):
    return os.path.abspath(directory)

###############################################################################
# test if this is a file and correct the path
###############################################################################
def test_is_directory(directory):
    directory = set_correct_path(directory)

    if not os.path.isdir(directory):
        logging.error('"%s" is not a directory', directory)
        raise Exception('"%s" is not a directory' % (directory))

    return directory
###############################################################################
def conv_combo_check_boxes(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    values = xml_data.value.list.find_all('text')
    for value in values:

        result.addCommand(
            ('waitForElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(
            ('assertElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(('click', 'name=%s%s value=%s' %
                           (id_path, name, value.attrs['id']), ''))
    return result

###############################################################################
def conv_combo_box_multiple_boxes(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    values = xml_data.value.list.find_all('text')
    for value in values:

        result.addCommand(
            ('waitForElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(
            ('assertElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(('addSelection', 'name=%s%s' %
                           (id_path, name), 'value=%s' % (value.attrs['id'])))
    return result

###############################################################################
def conv_combo_box(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.find('text').attrs['id']
    text = xml_data.value.find('text').text
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('waitForElementPresent',
                       "//select[@name='%s%s']/option[text()='%s']"
                       % (id_path, name, text), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('select', 'name=%s%s' %
                       (id_path, name), 'value=%s' % (value)))
    return result

###############################################################################
def conv_bool_combo_box(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.text
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('select', 'name=%s%s' %
                       (id_path, name), 'value=%s' % (value)))

    return result

###############################################################################
def conv_bool(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.text
    if value == 'yes':
        result.addCommand(
            ('waitForElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(
            ('assertElementPresent', 'name=%s%s' % (id_path, name), ''))
        result.addCommand(('click', 'name=%s%s value=%s' %
                           (id_path, name, value), ''))
    return result

###############################################################################
def conv_radio(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.find('text')
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('click', 'name=%s%s value=%s' %
                       (id_path, name, value.attrs['id']), ''))
    return result

###############################################################################
def conv_date(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.string
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s' % (id_path, name), value))
    return result

###############################################################################
def conv_date_range(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.list.find("date", {'id': "from"}).text
    result.addCommand(
        ('waitForElementPresent', 'name=%s%s.from' % (id_path, name), ''))
    result.addCommand(
        ('assertElementPresent', 'name=%s%s.from' % (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s.from' % (id_path, name), value))

    value = xml_data.value.list.find("date", {'id': "to"}).text
    result.addCommand(
        ('waitForElementPresent', 'name=%s%s.to' % (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s.to' %
                       (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s.to' % (id_path, name), value))

    return result

###############################################################################
def conv_phone(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.find_all('text')[2].text
    result.addCommand(
        ('waitForElementPresent',
         'name=%s%s.international' % (id_path, name),
         ''))
    result.addCommand(
        ('assertElementPresent',
         'name=%s%s.international' % (id_path, name),
         ''))
    result.addCommand(('type', 'name=%s%s.international' %
                       (id_path, name), value))
    return result

###############################################################################
def conv_file(xml_data, id_path=""):
    del id_path
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = os.path.join("""${attachmentsFolder}""",
                         xml_data.value.find('file').text)
    result.addCommand((
        'waitForElementPresent',
        'xpath=//*[@id="file-attachmentPreprocess_%s"]'
        '/div/div[2]/div[1]/div/input' % (name), ''))
    result.addCommand((
        'type',
        'xpath=//*[@id="file-attachmentPreprocess_%s"]'
        '/div/div[2]/div[1]/div/input' % (name), value))
    result.addCommand((
        'waitForElementPresent',
        'id=file-attachmentPreprocess_%s-1' % (name), ''))
    return result

###############################################################################
def conv_password(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.string
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s' % (id_path, name), value))

    result.addCommand(
        ('waitForElementPresent', 'name=%s%s.check' % (id_path, name), ''))
    result.addCommand(
        ('assertElementPresent', 'name=%s%s.check' % (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s.check' % (id_path, name), value))
    return result

###############################################################################
def conv_rating(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.string
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand((
        'click',
        '//*[@name="%s%s"]/following::a[%s]' % (id_path, name, value),
        ''))
    return result

###############################################################################
def conv_siren_api(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.list.find("text", {'id': "val"}).text
    result.addCommand(
        ('waitForElementPresent', 'name=%s%s.val' % (id_path, name), ''))
    result.addCommand(
        ('assertElementPresent', 'name=%s%s.val' % (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s.val' % (id_path, name), value))
    return result

###############################################################################
def conv_string(xml_data, id_path=""):
    result = krypton.TestCase()
    if not xml_data.find('value'):
        return result

    name = xml_data.attrs['id']
    value = xml_data.value.string
    result.addCommand(('waitForElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('assertElementPresent', 'name=%s%s' %
                       (id_path, name), ''))
    result.addCommand(('type', 'name=%s%s' % (id_path, name), value))
    return result


###############################################################################
__convTypeXmlToSelenese__ = {}
__convTypeXmlToSelenese__['bic'] = conv_string
__convTypeXmlToSelenese__['string'] = conv_string
__convTypeXmlToSelenese__['text'] = conv_string
__convTypeXmlToSelenese__['email'] = conv_string
__convTypeXmlToSelenese__['integer'] = conv_string
__convTypeXmlToSelenese__['iban'] = conv_string
__convTypeXmlToSelenese__['nir'] = conv_string
__convTypeXmlToSelenese__['rib'] = conv_string
__convTypeXmlToSelenese__['siren'] = conv_string
__convTypeXmlToSelenese__['sirenapientreprise'] = conv_siren_api
__convTypeXmlToSelenese__['siret'] = conv_string
__convTypeXmlToSelenese__['siretapientreprise'] = conv_siren_api
__convTypeXmlToSelenese__['rating'] = conv_rating
__convTypeXmlToSelenese__['passwordbase64'] = conv_password
__convTypeXmlToSelenese__['passwordbcrypt'] = conv_password
__convTypeXmlToSelenese__['datebirthday'] = conv_date
__convTypeXmlToSelenese__['datepast'] = conv_date
__convTypeXmlToSelenese__['datefuture'] = conv_date
__convTypeXmlToSelenese__['date'] = conv_date
__convTypeXmlToSelenese__['daterange'] = conv_date_range
__convTypeXmlToSelenese__['checkboxes'] = conv_combo_check_boxes
__convTypeXmlToSelenese__['combobox'] = conv_combo_box
__convTypeXmlToSelenese__['comboboxmultiple'] = conv_combo_box_multiple_boxes
__convTypeXmlToSelenese__['boolean'] = conv_bool
__convTypeXmlToSelenese__['booleancheckbox'] = conv_bool
__convTypeXmlToSelenese__['booleancombobox'] = conv_bool_combo_box
__convTypeXmlToSelenese__['radios'] = conv_radio
__convTypeXmlToSelenese__['phone'] = conv_phone
__convTypeXmlToSelenese__['file'] = conv_file


###############################################################################
# Convert to Selenium
###############################################################################
def conv_data_group_to_selenese(xml_data, xml_default):

    result = krypton.TestCase()
    id_root = xml_data.attrs['id']
    # print("id_root=%s"%id_root)
    datas = xml_data.find_all('data')
    for data in datas:
        # find the type
        dtypebrut = xml_default['type']

        if 'type' in data.attrs:
            dtypebrut = data.attrs['type']

        # find the path to define the id
        id_path = ""
        data_parent = data
        nb_loop = 0
        while (data_parent.parent != xml_data) and (nb_loop < 50):
            id_path = "%s." % (data_parent.parent.attrs['id']) + id_path
            data_parent = data_parent.parent
            nb_loop = nb_loop + 1

        # launch the conversion
        match_var = re.search(
            r"""(?P<type>\w+)(?:\((?P<param>.*):(?P<value>.+)\))?""",
            dtypebrut)
        dtype = match_var.group('type')
        if dtype.lower() in __convTypeXmlToSelenese__:
            result += __convTypeXmlToSelenese__[dtype.lower()](data, id_path)
        else:
            logging.info("------> type=%s", dtype)

    # Go to the next page
    if id_root == 'thanks':
        result.addCommand(
            ('waitForElementPresent', 'class=btn btn-success', ''))
        result.addCommand(('clickAndWait', 'class=btn btn-success', ''))
    else:
        result.addCommand(
            ('waitForElementPresent', 'class=btn btn-primary', ''))
        result.addCommand(('clickAndWait', 'class=btn btn-primary', ''))

    return result

###############################################################################
# Convert to Selenium
###############################################################################
def convert_data_xml_to_selenese(xml_data, profiler=False):
    xml = BeautifulSoup(xml_data, "lxml")
    logging.info("Convert to selenese : %s", xml.form.attrs['id'])

    xml_default = {}
    if xml.form.default is not None and xml.form.default.data is not None:
        xml_default = xml.form.default.data.attrs

    if 'type' not in xml_default:
        xml_default['type'] = None

    result = krypton.TestCase()
    result += krypton.testCaseSeparator()
    result += krypton.testCaseComment("Start the form %s" % (
        xml.form.attrs['label'] if ('label' in xml.form.attrs)
        else xml.form.attrs['id']))
    result += krypton.testCaseSeparator()

    groups = xml.form.find_all('group', recursive=False)
    nb_groups = len(groups)
    count = 0

    if profiler:
        group_to_process = groups[:-1]
    else:
        group_to_process = groups

    for group in group_to_process:
        count = count + 1
        result += krypton.testCaseSeparator()
        result += krypton.testCaseComment(
            "Start the page %d / %d : id=%s" %
            (count, nb_groups, str(group.attrs['id'])))
        result += krypton.testCaseSeparator()
        result.addCommand(('pause', '1000', ''))
        result += conv_data_group_to_selenese(group, xml_default)
    if profiler:
        result.addCommand(('pause', '1000', ''))
        result.addCommand(
            ('waitForElementPresent', 'class=btn btn-success', ''))
        result.addCommand(('clickAndWait', 'class=btn btn-success', ''))

    result += krypton.testCaseSeparator()
    return result

###############################################################################
# Convert to Selenium
###############################################################################
def conv_data_xml_file_to_selenese(filename):
    the_filename = commons.test_is_file_and_correct_path(filename)
    logging.info("Convert to selenese the file : %s", the_filename)
    file = open(the_filename)
    content = file.read()
    file.close()
    return convert_data_xml_to_selenese(content)

###############################################################################
# Main script
###############################################################################


def main():
    # ------------------------------------
    logging.info('Started%s ', commons.get_this_filename())
    logging.info('The Python version is %s', '.'.join(
        str(x) for x in sys.version_info[:-2]))

    testcase = conv_data_xml_file_to_selenese('data.xml')

    print(conv_data_xml_file_to_selenese('data.xml'))

    testcase.writeFile("essai.html")

    logging.info('Finished')
    # ------------------------------------


###############################################################################
# Call main if the script is main
###############################################################################
if __name__ == '__main__':
    main()
