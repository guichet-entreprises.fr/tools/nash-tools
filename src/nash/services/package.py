﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import os
import shutil
import zipfile
import repositorytools

from bs4 import BeautifulSoup
from urllib.parse import urlparse


def get_xml_install(list_forms, list_removed_formality):
    xml = BeautifulSoup("", "xml")
    package = xml.new_tag("package")
    install = xml.new_tag("publish")
    remove = xml.new_tag("unpublish")

    #  formality to install
    for form in list_forms:
        formality = xml.new_tag("formality")
        formality.string = os.path.basename(form.get_zip_filename())
        formality['group'] = form.get_group()
        install.append(formality)

    # formality to depublish
    for line in list_removed_formality.splitlines():
        if line:
            formality = xml.new_tag("formality")
            formality.string = line
            remove.append(formality)

    package.contents.append(install)
    package.contents.append(remove)
    xml.contents.append(package)
    return xml.prettify()


def checklist_formality(list_reference_id):
    # vérification de l'unicité des rfId de la liste

    if len(list_reference_id) > len(set(list_reference_id)):
        map_rfid = []
        list_rfid_doublon = []
        for rfid in list_reference_id:
            if rfid in map_rfid:
                list_rfid_doublon.append(rfid)
            else:
                map_rfid.append(rfid)
        raise Exception("duplicate error :\n\n" +
                        '\n'.join('{}'.format(k[1])
                                  for k in enumerate(list_rfid_doublon)))

def generate_package(pathname, list_formality, list_removed_formality=None):
    new_package = zipfile.ZipFile(pathname, 'w')
    list_reference_id = []
    i = 0

    for form in list_formality:
        new_zip_path, new_zip_name = os.path.split(
            form.get_zip_filename(build=True))
        form.rename_zip(os.path.join(
            new_zip_path, str(i) + "_" + new_zip_name))
        list_reference_id.append(form.get_reference_id())
        new_package.write(form.get_zip_filename(),
                          os.path.basename(form.get_zip_filename()))
        form.clean()
        i = i + 1

    checklist_formality(list_reference_id)

    # prefix for multiple forms who shares the same name

    new_package.writestr("package.xml", get_xml_install(
        list_formality, list_removed_formality))
    new_package.close()


def upload(pathname, infos):

    url = infos['url']
    user = infos['user']
    password = infos['password']

    if(not url.startswith("http") and not url.startswith("//")):
        url = "//" + url
    url_object = urlparse(url)
    if(url_object.scheme == "http" or url_object.scheme == ""):
        url_object = url_object._replace(scheme="https")
    url = url_object.geturl()

    artifact = repositorytools.LocalArtifact(
        local_path=pathname, group='fr.ge.formalities', version='')
    client = repositorytools.repository_client_factory(
        repository_url=url, user=user, password=password)
    remote_artifacts = client.upload_artifacts(
        local_artifacts=[artifact], repo_id='formalities')

    return remote_artifacts[0].url
