﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx


class LoginDialogIde(wx.Dialog):
    """
    Class to define login dialog
    """

    #----------------------------------------------------------------------
    def __init__(self, parent=None, title="Login"):
        """Constructor"""
        super(LoginDialogIde, self).__init__(parent, title=title)

        self.main_panel = wx.Panel(self)
        self.panel_formality_infos = wx.Panel(self.main_panel)
        self.panel_button = wx.Panel(self.main_panel)

        self.SetSize(280, 240)

        # username
        user_label = wx.StaticText(
            self.panel_formality_infos, label="Username:")
        self.user = wx.TextCtrl(self.panel_formality_infos)

        user_sizer = wx.BoxSizer(wx.HORIZONTAL)
        user_sizer.Add(user_label, 0, wx.ALL | wx.CENTER, 5)
        user_sizer.Add(self.user, 0, wx.ALL, 5)

        # password
        password_label = wx.StaticText(
            self.panel_formality_infos, label="Password:")
        self.password = wx.TextCtrl(
            self.panel_formality_infos,
            style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER)

        # check box remember
        self.remember = wx.CheckBox(
            self.panel_formality_infos, label='Remember user and password')

        password_sizer = wx.BoxSizer(wx.HORIZONTAL)
        password_sizer.Add(password_label, 0, wx.ALL | wx.CENTER, 5)
        password_sizer.Add(self.password, 0, wx.ALL, 5)
        password_sizer.Add(self.remember, 0, wx.ALL, 5)

        # boutons
        button_login = wx.Button(self.panel_button, wx.ID_OK, label="Login")
        button_cancel = wx.Button(
            self.panel_button, wx.ID_CANCEL, label="Cancel")

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(button_login, 0, wx.ALL | wx.CENTER, 5)
        button_sizer.Add(button_cancel, 0, wx.ALL | wx.CENTER, 5)

        infos_sizer = wx.BoxSizer(wx.VERTICAL)
        infos_sizer.Add(user_sizer, 0, wx.ALL, 5)
        infos_sizer.Add(password_sizer, 0, wx.ALL, 5)
        infos_sizer.Add(self.remember, 0, wx.ALL, 5)

        self.panel_formality_infos.SetSizerAndFit(infos_sizer)
        self.panel_button.SetSizerAndFit(button_sizer)
        self.dialogue_sizer = wx.BoxSizer(wx.VERTICAL)
        self.dialogue_sizer.Add(self.panel_formality_infos, 1, flag=wx.EXPAND)
        self.dialogue_sizer.Add(self.panel_button, 0, wx.ALL | wx.CENTER, 5)

        self.main_panel.SetSizerAndFit(self.dialogue_sizer)
        self.SetBackgroundColour("White")
        self.Show()

    def get_result(self):
        password = self.password.GetValue()
        user = self.user.GetValue()
        remember = self.remember.GetValue()
        return {'user': user, 'password': password, 'remember': remember}
