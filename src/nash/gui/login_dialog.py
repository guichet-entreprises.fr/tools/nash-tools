#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx

import nash.services.commons as commons


class LoginDialog(wx.App):
    """
    Class to define login dialog
    """

    def __init__(self, title="Login", parent=None, size=(400, 300)):
        """Constructor"""
        wx.App.__init__(self, None)

        self.result = None

        self.frame = wx.Frame(parent, title=title, size=size)
        # user info
        user_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # username
        user_lbl = wx.StaticText(self.frame, label="Username:")
        user_sizer.Add(user_lbl, 0, wx.ALL | wx.CENTER, 5)
        self.user = wx.TextCtrl(self.frame)
        user_sizer.Add(self.user, 0, wx.ALL, 5)

        # pass info
        pwd_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # password
        pwd_lbl = wx.StaticText(self.frame, label="Password:")
        pwd_sizer.Add(pwd_lbl, 0, wx.ALL | wx.CENTER, 5)
        self.password = wx.TextCtrl(
            self.frame, style=wx.TE_PASSWORD | wx.TE_PROCESS_ENTER)
        pwd_sizer.Add(self.password, 0, wx.ALL, 5)

        main_sizer = wx.BoxSizer(wx.VERTICAL)
        main_sizer.Add(user_sizer, 0, wx.ALL, 5)
        main_sizer.Add(pwd_sizer, 0, wx.ALL, 5)

        # check box remember
        self.remember = wx.CheckBox(
            self.frame, label='Remember user and password')

        main_sizer.Add(self.remember, 0, wx.ALL, 5)

        # boutons
        b_sizer = wx.BoxSizer(wx.HORIZONTAL)
        btn_login = wx.Button(self.frame, label="Login")
        btn_login.Bind(wx.EVT_BUTTON, self.on_login)

        b_sizer.Add(btn_login, 0, wx.ALL | wx.CENTER, 5)

        btn_cancel = wx.Button(self.frame, label="Cancel")
        btn_cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

        b_sizer.Add(btn_cancel, 0, wx.ALL | wx.CENTER, 5)

        main_sizer.Add(b_sizer, 0, wx.ALL, 5)

        self.frame.SetSizer(main_sizer)
        self.frame.SetBackgroundColour("White")
        self.frame.SetSize(226, 195)
        self.frame.Show()

    def on_cancel(self, event):
        del event
        self.result = commons.LOGIN_CANCEL
        self.frame.Destroy()

    def on_login(self, event):
        del event
        password = self.password.GetValue()
        user = self.user.GetValue()
        remember = self.remember.GetValue()
        self.result = {'user': user,
                       'password': password, 'remember': remember}
        self.frame.Destroy()

    def pick(self):
        self.MainLoop()
        return self.result


def launch_login_dialog(title="login"):
    _mon_app = LoginDialog(title)
    _result = _mon_app.pick()
    return _result
