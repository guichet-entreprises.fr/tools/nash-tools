﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx

import nash.services.jira_connection as jira_connection

# import wx.lib.inspection


class JiraCreateDialog(wx.Dialog):
    def __init__(self, parent, title="title"):
        super(JiraCreateDialog, self).__init__(parent, title=title,
                                               style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.width = 400
        self.height = 300
        self.SetSize(wx.Size(self.width, self.height))

        main_panel = wx.Panel(self)
        dialogue_sizer = wx.BoxSizer(wx.VERTICAL)

        list_project = ['SAND', 'FF']
        label_project = wx.StaticText(
            main_panel, label="créer les fiches dans le projet :")
        self.project = wx.ComboBox(
            main_panel, size=(140, -1), choices=list_project)

        status = ['Open', 'Close']
        label_status = wx.StaticText(
            main_panel, label="créer les fiches au status :")
        self.status = wx.ComboBox(main_panel, size=(140, -1), choices=status)

        label_sprint = wx.StaticText(main_panel, label="sprint :")
        self.sprint = wx.TextCtrl(main_panel, size=(140, -1))

        label_user = wx.StaticText(main_panel, label="créateur :")

        user_jira = jira_connection.JiraConnection().user
        self.user = wx.StaticText(main_panel, label=user_jira)

        self.btn_ok = wx.Button(main_panel, wx.ID_OK,
                                label="ok", size=(55, 30))
        self.btn_ok.SetMaxSize(wx.Size(55, 30))
        self.btn_cancel = wx.Button(
            main_panel, wx.ID_CANCEL, label="Cancel", size=(55, 30))
        self.btn_cancel.SetMaxSize(wx.Size(55, 30))

        # static_box_infos
        static_box_infos = wx.StaticBox(
            main_panel, wx.ID_ANY, " JIRA informations ")
        static_box_infos_sizer = wx.StaticBoxSizer(
            static_box_infos, wx.VERTICAL)
        grid_sizer = wx.GridBagSizer(4, 4)
        grid_sizer.Add(label_project, (0, 0))
        grid_sizer.Add(self.project, (0, 1), flag=wx.ALL |
                       wx.ALIGN_CENTER_HORIZONTAL)
        grid_sizer.Add(label_status, (1, 0))
        grid_sizer.Add(self.status, (1, 1), flag=wx.ALL |
                       wx.ALIGN_CENTER_HORIZONTAL)
        grid_sizer.Add(label_sprint, (2, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        grid_sizer.Add(self.sprint, (2, 1))
        grid_sizer.Add(label_user, (3, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        grid_sizer.Add(self.user, (3, 1))

        static_box_infos_sizer.Add(grid_sizer, 0, wx.ALL | wx.CENTER, 10)

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(self.btn_ok, 0, wx.ALL, 5)
        button_sizer.Add(self.btn_cancel, 0, wx.ALL, 5)

        dialogue_sizer.Add(static_box_infos_sizer, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(button_sizer, 0, wx.ALL | wx.CENTER, 5)
        main_panel.SetSizer(dialogue_sizer)
        main_panel.Fit()
        # wx.lib.inspection.InspectionTool().Show()

    def get_status(self):
        return self.status.GetValue()

    def get_project(self):
        return self.project.GetValue()

    def get_sprint(self):
        return self.sprint.GetValue()

def main():
    app = wx.App()
    frm = JiraCreateDialog(None, title='mytitle')
    frm.Show()
    app.MainLoop()


###############################################################################
# main script
###############################################################################
if __name__ == '__main__':
    main()
