﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import os
import wx
import wx.adv
import requests
import nash.services.jira_connection as jira_connection
# import wx.lib.inspection


class PackageDialog(wx.Dialog):
    def __init__(self, parent, title="title", use_jira=False):
        super(PackageDialog, self).__init__(
            parent, title=title,
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.width = 315
        self.height = 550
        self.SetSize(wx.Size(self.width, self.height))

        main_panel = wx.Panel(self)
        dialogue_sizer = wx.BoxSizer(wx.VERTICAL)

        apps = ['GE', 'GQ', 'Profiler', 'support']
        project_jira = []

        label_apps = wx.StaticText(main_panel, label="Application :")
        self.__combo_apps = wx.ComboBox(
            main_panel, size=(140, -1), choices=apps)

        label_version = wx.StaticText(main_panel, label="Version :")
        self.__version = wx.TextCtrl(main_panel, size=(140, -1))
        label_group = wx.StaticText(main_panel, label="group (optional) :")
        self.__group = wx.TextCtrl(main_panel, size=(140, -1))
        self.__nexus = wx.CheckBox(main_panel,
                                   label='Livraison sur NEXUS')
        self.__bl_jira = wx.CheckBox(main_panel,
                                     label='Générer un BL JIRA')
        self.__combo_project = wx.adv.BitmapComboBox(
            main_panel, size=(140, -1), style=wx.CB_READONLY | wx.CB_DROPDOWN)
        label_unpublishing = wx.StaticText(main_panel,
                                           label="one reference-id per line :")
        self.__unpublishing = wx.TextCtrl(main_panel,
                                          style=wx.TE_MULTILINE | wx.HSCROLL)

        self.btn_ok = wx.Button(main_panel, wx.ID_OK,
                                label="ok", size=(55, 30))
        self.btn_ok.SetMaxSize(wx.Size(55, 30))
        self.btn_cancel = wx.Button(main_panel, wx.ID_CANCEL,
                                    label="Cancel", size=(55, 30))
        self.btn_cancel.SetMaxSize(wx.Size(55, 30))

        # static_box_infos
        static_box_infos = wx.StaticBox(main_panel,
                                        wx.ID_ANY,
                                        " Package informations ")
        static_box_infos_sizer = wx.StaticBoxSizer(
            static_box_infos, wx.VERTICAL)
        grid_sizer = wx.GridBagSizer(6, 2)
        grid_sizer.Add(label_apps, (0, 0))
        grid_sizer.Add(self.__combo_apps, (0, 1),
                       flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
        grid_sizer.Add(label_version, (1, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        grid_sizer.Add(self.__version, (1, 1))
        grid_sizer.Add(label_group, (2, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        grid_sizer.Add(self.__group, (2, 1))
        grid_sizer.Add(self.__nexus, (3, 0), (3, 2))

        static_box_infos_sizer.Add(grid_sizer, 0, wx.ALL | wx.CENTER, 10)

        # static_box_unpublishing
        static_box_unpublishing = wx.StaticBox(
            main_panel, wx.ID_ANY, " unpublishing ")
        static_box_unpublishing_sizer = wx.StaticBoxSizer(
            static_box_unpublishing, wx.VERTICAL)
        unpublishing_sizer = wx.BoxSizer(wx.VERTICAL)
        unpublishing_sizer.Add(label_unpublishing, 0, wx.LEFT, 5)
        unpublishing_sizer.Add(
            self.__unpublishing, 1, wx.EXPAND | wx.ALL | wx.CENTER, 5)

        static_box_unpublishing_sizer.Add(
            unpublishing_sizer, 1, wx.EXPAND | wx.ALL | wx.CENTER, 10)

        # static_box_jira
        static_box_jira = wx.StaticBox(main_panel, wx.ID_ANY, " Jira ")
        static_box_jira_sizer = wx.StaticBoxSizer(static_box_jira, wx.VERTICAL)

        grid_sizer = wx.GridBagSizer(1, 2)
        grid_sizer.Add(self.__bl_jira, (0, 0))
        grid_sizer.Add(self.__combo_project, (0, 1))
        static_box_jira_sizer.Add(grid_sizer, 0, wx.ALL | wx.CENTER, 5)

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(self.btn_ok, 0, wx.ALL, 5)
        button_sizer.Add(self.btn_cancel, 0, wx.ALL, 5)

        dialogue_sizer.Add(static_box_infos_sizer, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(static_box_unpublishing_sizer, 1,
                           wx.EXPAND | wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(static_box_jira_sizer, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(button_sizer, 0, wx.ALL | wx.CENTER, 5)

        wx.EVT_BUTTON(self, wx.ID_OK, self.on_ok)

        if not use_jira:
            self.__bl_jira.Disable()
        else:
            self.construct_combo_project()

        self.__combo_project.Disable()
        self.__bl_jira.Bind(wx.EVT_CHECKBOX, self.on_item_checked)

        main_panel.SetSizer(dialogue_sizer)
        main_panel.Fit()
        # wx.lib.inspection.InspectionTool().Show()

    def on_item_checked(self, _):
        if self.__bl_jira.IsChecked():
            self.__combo_project.Enable()
        else:
            self.__combo_project.Disable()

    def construct_combo_project(self,):
        results = []
        image = wx.Bitmap(wx.Image(os.path.join(
            'img', 'zip.ico'), wx.BITMAP_TYPE_ANY))
        projects = jira_connection.JiraConnection().get_jira_project()

        for project in projects:
            self.__combo_project.Append(project['key'], project['image'])

        return results

    def on_ok(self, event):
        if self.generate_bl_jira and self.bl_jira_project == '':
            wx.MessageBox("Veuillez choisir un projet JIRA",
                          "Bon de livraison",
                          wx.OK | wx.ICON_ERROR)
            return

        self.EndModal(wx.ID_OK)

    @property
    def version(self):
        return self.__version.GetValue()

    @property
    def group(self):
        return self.__group.GetValue()

    @property
    def app(self):
        return self.__combo_apps.GetValue().lower()

    @property
    def formality_to_unpublish(self):
        return self.__unpublishing.GetValue()

    @property
    def nexus(self):
        return self.__nexus.GetValue()

    @property
    def generate_bl_jira(self):
        return self.__bl_jira.GetValue()

    @property
    def bl_jira_project(self):
        return self.__combo_project.GetValue()
