﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx
import wx.lib.scrolledpanel

import nash.services.commons as commons
import nash.gui.item_marketplace as item_marketplace


class Marketplace(wx.Dialog):
    def __init__(self, parent, title="title"):
        super(Marketplace, self).__init__(
            parent, title=title,
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.width = 690
        self.height = 690
        self.SetSize(wx.Size(self.width, self.height))

        self.main_panel = wx.Panel(self)
        dialogue_sizer = wx.BoxSizer(wx.VERTICAL)

        # top items
        self.search = wx.SearchCtrl(self.main_panel)

        top_sizer = wx.BoxSizer(wx.HORIZONTAL)
        top_sizer.Add(self.search, 0, wx.ALL, 5)

        # forms items
        self.forms_pannel = wx.lib.scrolledpanel.ScrolledPanel(self)
        self.forms_sizer = wx.BoxSizer(wx.VERTICAL)
        self.forms_pannel.SetSizer(self.forms_sizer)
        # buttons
        self.btn_ok = wx.Button(self.main_panel, wx.ID_OK,
                                label="ok", size=(55, 30))
        self.btn_ok.SetMaxSize(wx.Size(55, 30))
        self.btn_cancel = wx.Button(
            self.main_panel, wx.ID_CANCEL, label="Cancel", size=(55, 30))
        self.btn_cancel.SetMaxSize(wx.Size(55, 30))

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(self.btn_ok, 0, wx.ALL, 5)
        button_sizer.Add(self.btn_cancel, 0, wx.ALL, 5)

        dialogue_sizer.Add(top_sizer, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(self.forms_pannel, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(button_sizer, 0, wx.ALL | wx.CENTER, 5)
        self.main_panel.SetSizer(dialogue_sizer)
        self.main_panel.Fit()

        self.btn_ok.Bind(wx.EVT_BUTTON, self.on_ok)

        self.init_forms_list()

    def on_ok(self, event):
        del event

        self.Destroy()

    def init_forms_list(self):
        item_one = item_marketplace.ItemMarketplace(self.forms_pannel)
        item_two = item_marketplace.ItemMarketplace(self.forms_pannel)
        item_three = item_marketplace.ItemMarketplace(self.forms_pannel)
        item_four = item_marketplace.ItemMarketplace(self.forms_pannel)

        self.forms_sizer.Add(item_one, 0, wx.ALL |
                             wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        self.forms_sizer.Add(item_two, 0, wx.ALL |
                             wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        self.forms_sizer.Add(item_three, 0, wx.ALL |
                             wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        self.forms_sizer.Add(item_four, 0, wx.ALL |
                             wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)


if __name__ == '__main__':
    # When this module is run (not imported) then create the app, the
    # frame, show it, and start the event loop.
    commons.set_logging_system()
    __app_nash__ = wx.App()
    __frm_nash__ = Marketplace(None, title='marketplace')
    __frm_nash__.Show()
    __frm_nash__.Centre()
    __app_nash__.MainLoop()
