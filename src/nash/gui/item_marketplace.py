#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx


class ItemMarketplace(wx.Panel):

    def __init__(self, parent, forms=None):
        super(ItemMarketplace, self).__init__(parent, style=wx.SIMPLE_BORDER)
        del forms
        self.icone = None
        self.name = None
        self.author = None
        self.rating = None

        box_sizer = wx.BoxSizer(wx.VERTICAL)

        self.icone = wx.StaticText(self, label="""
|) _ |_|
| (_| _|
""")
        self.name = wx.StaticText(self, label="Payment")
        self.author = wx.StaticText(self, label="Arnaud Boidard")
        self.rating = wx.StaticText(self, label="★★★★☆")
        box_sizer.Add(self.icone, 0, wx.ALL | wx.CENTER |
                      wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        box_sizer.Add(self.name, 0, wx.ALL | wx.CENTER |
                      wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        box_sizer.Add(self.author, 0, wx.ALL | wx.CENTER |
                      wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        box_sizer.Add(self.rating, 0, wx.ALL | wx.CENTER |
                      wx.RESERVE_SPACE_EVEN_IF_HIDDEN, 5)
        self.SetSizer(box_sizer)
