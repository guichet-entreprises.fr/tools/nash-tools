﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx


class RegentDialog(wx.Dialog):
    def __init__(self, parent, title="title"):
        super(RegentDialog, self).__init__(
            parent, title=title,
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.width = 400
        self.height = 200
        self.SetSize(wx.Size(self.width, self.height))

        main_panel = wx.Panel(self)
        dialogue_sizer = wx.BoxSizer(wx.VERTICAL)

        label_version = wx.StaticText(main_panel, label="version du regent :")
        self._version = wx.TextCtrl(main_panel, size=(140, -1))
        label_events = wx.StaticText(
            main_panel, label="Evenements (séparés par des virgules) :")
        self._events = wx.TextCtrl(main_panel, size=(140, -1))

        self.btn_ok = wx.Button(main_panel, wx.ID_OK,
                                label="ok", size=(55, 30))
        self.btn_ok.SetMaxSize(wx.Size(55, 30))
        self.btn_cancel = wx.Button(
            main_panel, wx.ID_CANCEL, label="Cancel", size=(55, 30))
        self.btn_cancel.SetMaxSize(wx.Size(55, 30))

        # static_box_infos
        static_box_infos = wx.StaticBox(
            main_panel, wx.ID_ANY, " REGENT informations ")
        static_box_infos_sizer = wx.StaticBoxSizer(
            static_box_infos, wx.VERTICAL)
        grid_sizer = wx.GridBagSizer(2, 2)
        grid_sizer.Add(label_version, (0, 0))
        grid_sizer.Add(self._version, (0, 1), flag=wx.ALL |
                       wx.ALIGN_CENTER_HORIZONTAL)
        grid_sizer.Add(label_events, (1, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        grid_sizer.Add(self._events, (1, 1))

        static_box_infos_sizer.Add(grid_sizer, 0, wx.ALL | wx.CENTER, 10)

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(self.btn_ok, 0, wx.ALL, 5)
        button_sizer.Add(self.btn_cancel, 0, wx.ALL, 5)

        dialogue_sizer.Add(static_box_infos_sizer, 0, wx.ALL | wx.CENTER, 5)
        dialogue_sizer.Add(button_sizer, 0, wx.ALL | wx.CENTER, 5)
        main_panel.SetSizer(dialogue_sizer)
        main_panel.Fit()

        self._version.SetValue("v2008.11")
        # wx.lib.inspection.InspectionTool().Show()

    @property
    def version(self):
        return self._version.GetValue().upper()

    @property
    def list_events(self):
        result = self._events.GetValue().replace(" ", "").upper().split(",")

        return result

###############################################################################
# main script
###############################################################################
def main():
    app = wx.App()
    frm = RegentDialog(None, title='mytitle')
    frm.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
