﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx
import wx.lib.agw.flatnotebook as fnb
# import wx.lib.inspection

from jira import JIRAError

import nash.services.commons as commons
import nash.services.jira_connection as jira_connection

class MainPanel(wx.Panel):
    def construct_package(self, panel):
        package_panel = wx.Panel(panel, name="package_panel")
        static_box_package = wx.StaticBox(
            package_panel, wx.ID_ANY, " Package ")
        static_box_package_sizer = wx.StaticBoxSizer(
            static_box_package, wx.VERTICAL)

        self.label_package_url = wx.StaticText(package_panel, label="url :")
        self.package_url = wx.TextCtrl(package_panel, size=(180, -1))
        self.label_package_user = wx.StaticText(package_panel, label="user :")
        self.package_user = wx.TextCtrl(package_panel, size=(140, -1))
        self.label_package_password = wx.StaticText(
            package_panel, label="password :")
        self.package_password = wx.TextCtrl(
            package_panel, size=(140, -1), style=wx.TE_PASSWORD)

        package_grid_sizer = wx.GridBagSizer(3, 2)
        package_grid_sizer.Add(self.label_package_url, (0, 0),
                               flag=wx.ALL | wx.ALIGN_RIGHT)
        package_grid_sizer.Add(self.package_url, (0, 1))
        package_grid_sizer.Add(self.label_package_user, (1, 0),
                               flag=wx.ALL | wx.ALIGN_RIGHT)
        package_grid_sizer.Add(self.package_user, (1, 1))
        package_grid_sizer.Add(self.label_package_password,
                               (2, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        package_grid_sizer.Add(self.package_password, (2, 1))
        static_box_package_sizer.Add(
            package_grid_sizer, 0, wx.ALL | wx.CENTER, 5)

        package_panel.SetSizer(static_box_package_sizer)
        return package_panel

    def construct_uplaod(self, panel):
        upload_panel = wx.Panel(panel, name="upload_panel")
        upload_sizer = wx.BoxSizer(wx.VERTICAL)

        # upload box
        static_box_upload = wx.StaticBox(upload_panel, wx.ID_ANY, " Upload ")
        static_box_upload_sizer = wx.StaticBoxSizer(
            static_box_upload, wx.VERTICAL)

        self.label_upload_url = wx.StaticText(upload_panel, label="url :")
        self.upload_url = wx.TextCtrl(upload_panel, size=(500, -1))
        self.label_upload_user = wx.StaticText(upload_panel, label="user :")
        self.upload_user = wx.TextCtrl(upload_panel, size=(140, -1))
        self.label_upload_password = wx.StaticText(
            upload_panel, label="password :")
        self.upload_password = wx.TextCtrl(
            upload_panel, size=(140, -1), style=wx.TE_PASSWORD)
        self.label_upload_group = wx.StaticText(upload_panel, label="group :")
        self.upload_group = wx.TextCtrl(upload_panel, size=(140, -1))
        self.upload_publish = wx.CheckBox(
            upload_panel, label='Publier les Formalités lors de l\'upload')
        self.label_upload_author = wx.StaticText(
            upload_panel, label="author :")
        self.upload_author = wx.TextCtrl(upload_panel, size=(140, -1))

        upload_grid_sizer = wx.GridBagSizer(7, 2)
        upload_grid_sizer.Add(self.label_upload_url, (0, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.upload_url, (0, 1), flag=wx.EXPAND)
        upload_grid_sizer.Add(self.label_upload_user, (1, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.upload_user, (1, 1))
        upload_grid_sizer.Add(self.label_upload_password,
                              (2, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.upload_password, (2, 1))
        upload_grid_sizer.Add(self.label_upload_group, (3, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.upload_group, (3, 1))
        upload_grid_sizer.Add(self.upload_publish, (4, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.label_upload_author, (5, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        upload_grid_sizer.Add(self.upload_author, (5, 1))
        static_box_upload_sizer.Add(
            upload_grid_sizer, 0, wx.ALL | wx.CENTER, 6)

        # conf box
        static_box_conf = wx.StaticBox(
            upload_panel, wx.ID_ANY, " Load, save or delete a configuration ")
        self.label_conf_saved = wx.StaticText(
            upload_panel, label="Saved configuration")
        self.conf_name = wx.TextCtrl(upload_panel, size=(140, -1))
        self.list_box_conf = wx.ListBox(
            upload_panel, size=(140, 120), style=wx.LB_SINGLE)
        self.btn_conf_save = wx.Button(
            upload_panel, label="Save", size=(73, 23))
        self.btn_conf_delete = wx.Button(
            upload_panel, label="Delete", size=(73, 23))
        self.btn_conf_default = wx.Button(
            upload_panel, label="Default", size=(73, 23))
        self.btn_conf_new = wx.Button(
            upload_panel, label="New", size=(73, 23))

        static_box_conf_sizer = wx.StaticBoxSizer(
            static_box_conf, wx.HORIZONTAL)
        right_sizer = wx.BoxSizer(wx.VERTICAL)
        left_sizer = wx.BoxSizer(wx.VERTICAL)
        left_sizer.Add(self.label_conf_saved, wx.SizerFlags().Right())
        left_sizer.Add(
            self.conf_name, wx.SizerFlags().Right().Border(wx.BOTTOM, 4))
        left_sizer.Add(self.list_box_conf, wx.SizerFlags().Right())

        right_sizer.Add(self.btn_conf_new)
        right_sizer.Add(self.btn_conf_save)
        right_sizer.Add(self.btn_conf_delete)
        right_sizer.Add(self.btn_conf_default)

        static_box_conf_sizer.Add(
            left_sizer, wx.SizerFlags().Proportion(1))
        static_box_conf_sizer.Add(
            right_sizer, wx.SizerFlags().Proportion(1).Border(wx.TOP, 42))

        # mount sizers
        upload_sizer.Add(static_box_upload_sizer,
                         wx.SizerFlags().Expand())
        upload_sizer.Add(static_box_conf_sizer,
                         wx.SizerFlags().Expand())
        upload_panel.SetSizer(upload_sizer)

        # bind event

        return upload_panel

    def construct_regent(self, panel):
        regent_panel = wx.Panel(panel, name="regent_panel")
        static_box_regent = wx.StaticBox(regent_panel, wx.ID_ANY, " Regent ")
        static_box_regent_sizer = wx.StaticBoxSizer(
            static_box_regent, wx.VERTICAL)

        self.label_regent_url = wx.StaticText(regent_panel, label="url :")
        self.regent_url = wx.TextCtrl(regent_panel, size=(180, -1))
        self.label_regent_user = wx.StaticText(regent_panel, label="user :")
        self.regent_user = wx.TextCtrl(regent_panel, size=(140, -1))
        self.label_regent_password = wx.StaticText(
            regent_panel, label="password :")
        self.regent_password = wx.TextCtrl(
            regent_panel, size=(140, -1), style=wx.TE_PASSWORD)

        regent_grid_sizer = wx.GridBagSizer(3, 2)
        regent_grid_sizer.Add(self.label_regent_url, (0, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        regent_grid_sizer.Add(self.regent_url, (0, 1))
        regent_grid_sizer.Add(self.label_regent_user, (1, 0),
                              flag=wx.ALL | wx.ALIGN_RIGHT)
        regent_grid_sizer.Add(self.regent_user, (1, 1))
        regent_grid_sizer.Add(self.label_regent_password,
                              (2, 0), flag=wx.ALL | wx.ALIGN_RIGHT)
        regent_grid_sizer.Add(self.regent_password, (2, 1))
        static_box_regent_sizer.Add(
            regent_grid_sizer, 0, wx.ALL | wx.CENTER, 5)

        regent_panel.SetSizer(static_box_regent_sizer)
        return regent_panel

    def construct_jira(self, panel):
        jira_panel = wx.Panel(panel, name="jira_panel")
        static_box_jira = wx.StaticBox(jira_panel, wx.ID_ANY, " Jira ")
        static_box_jira_sizer = wx.StaticBoxSizer(static_box_jira, wx.VERTICAL)

        self.label_jira_url = wx.StaticText(jira_panel, label="url :")
        self.jira_url = wx.TextCtrl(jira_panel, size=(180, -1))
        self.label_jira_user = wx.StaticText(jira_panel, label="user :")
        self.jira_user = wx.TextCtrl(jira_panel, size=(140, -1))
        self.label_jira_password = wx.StaticText(
            jira_panel, label="password :")
        self.jira_password = wx.TextCtrl(
            jira_panel, size=(140, -1), style=wx.TE_PASSWORD)
        self.use_jira = wx.CheckBox(
            jira_panel, label='utiliser la connexion avec JIRA (BETA)')

        jira_grid_sizer = wx.GridBagSizer(4, 2)
        jira_grid_sizer.Add(self.label_jira_url, (0, 0),
                            flag=wx.ALL | wx.ALIGN_RIGHT)
        jira_grid_sizer.Add(self.jira_url, (0, 1))
        jira_grid_sizer.Add(self.label_jira_user, (1, 0),
                            flag=wx.ALL | wx.ALIGN_RIGHT)
        jira_grid_sizer.Add(self.jira_user, (1, 1))
        jira_grid_sizer.Add(self.label_jira_password, (2, 0),
                            flag=wx.ALL | wx.ALIGN_RIGHT)
        jira_grid_sizer.Add(self.jira_password, (2, 1))
        jira_grid_sizer.Add(self.use_jira, (3, 0), (1, 2))
        static_box_jira_sizer.Add(jira_grid_sizer, 0, wx.ALL | wx.CENTER, 5)

        jira_panel.SetSizer(static_box_jira_sizer)
        return jira_panel

    def construct_bottom(self):
        bottom_panel = wx.Panel(self, name="bottom_panel")
        self.btn_ok = wx.Button(bottom_panel, wx.ID_OK,
                                label="Ok", size=(73, 23))
        self.btn_cancel = wx.Button(bottom_panel, wx.ID_CANCEL,
                                    label="Cancel", size=(73, 23))

        button_sizer = wx.BoxSizer(wx.HORIZONTAL)
        button_sizer.Add(
            self.btn_ok, wx.SizerFlags().Expand().Border(wx.RIGHT, 10))
        button_sizer.Add(
            self.btn_cancel, wx.SizerFlags().Expand())
        bottom_panel.SetSizerAndFit(button_sizer)
        return bottom_panel

    def __init__(self, parent):
        self.parent = parent
        super().__init__(parent)

        tabs = fnb.FlatNotebook(
            self, agwStyle=fnb.FNB_NO_X_BUTTON |
            fnb.FNB_VC8 |
            fnb.FNB_SMART_TABS |
            fnb.FNB_NO_NAV_BUTTONS)
        tabs.AddPage(self.construct_uplaod(tabs), "Upload")
        tabs.AddPage(self.construct_jira(tabs), "Jira")
        tabs.AddPage(self.construct_package(tabs), "Package")
        tabs.AddPage(self.construct_regent(tabs), "Regent")

        frame_sizer = wx.BoxSizer(wx.VERTICAL)
        frame_sizer.Add(
            tabs, wx.SizerFlags().Left().Expand().Proportion(-1))
        frame_sizer.Add(self.construct_bottom(),
                        wx.SizerFlags().Center().Border(wx.ALL, 10))

        self.btn_ok.Bind(wx.EVT_BUTTON, self.on_ok)
        self.btn_conf_new.Bind(wx.EVT_BUTTON, self.on_new_conf)
        self.btn_conf_delete.Bind(wx.EVT_BUTTON, self.on_delete_conf)
        self.btn_conf_default.Bind(wx.EVT_BUTTON, self.on_default_conf)
        self.btn_conf_save.Bind(wx.EVT_BUTTON, self.on_save_conf)
        self.list_box_conf.Bind(
            wx.EVT_LISTBOX, self.on_sel_list_box_conf_change)

        self.SetSizerAndFit(frame_sizer)

        self.removed_conf = []
        self.default_section_name = None

        self.init_field()

        self.list_box_conf.Select(
            self.list_conf.index(self.default_section_name))

        self.upload_url.Bind(wx.EVT_TEXT, self.on_change)
        self.upload_user.Bind(wx.EVT_TEXT, self.on_change)
        self.upload_password.Bind(wx.EVT_TEXT, self.on_change)
        self.upload_publish.Bind(wx.EVT_CHECKBOX, self.on_change)
        self.upload_author.Bind(wx.EVT_TEXT, self.on_change)
        self.upload_group.Bind(wx.EVT_TEXT, self.on_change)

        self.change = False

    def on_sel_list_box_conf_change(self, _):
        item = self.list_box_conf.GetSelection()
        self.refresh_param_upload(self.list_box_conf.GetClientData(item))

    def on_change(self, _):
        self.change = True

    def on_new_conf(self, _):
        self.conf_name.SetValue("new Configuration")
        self.upload_user.SetValue("")
        self.upload_password.SetValue("")
        self.upload_url.SetValue("")
        self.upload_group.SetValue("")
        self.upload_publish.SetValue(False)
        self.upload_author.SetValue("")
        name_conf_to_save = "UPLOAD_CONF" + \
            str(int(self.list_conf[-1].replace("UPLOAD_CONF", "")) + 1)
        self.list_conf.append(name_conf_to_save)

        self.config.add_section(name_conf_to_save)
        self.config.set(name_conf_to_save, 'name', "new Configuration")
        self.config.set(name_conf_to_save, 'user', "")
        self.config.set(name_conf_to_save, 'password', "")
        self.config.set(name_conf_to_save, 'url', "")
        self.config.set(name_conf_to_save, 'group', "")
        self.config.set(name_conf_to_save, 'publish', "False")
        self.config.set(name_conf_to_save, 'author', "")

        self.refresh_list_conf()
        self.list_box_conf.Select(self.list_box_conf.GetCount() - 1)

    def on_delete_conf(self, _):
        if self.list_box_conf.Count < 2:
            return

        item = self.list_box_conf.GetSelection()
        name_conf_to_delete = self.list_box_conf.GetClientData(item)

        if name_conf_to_delete == self.default_section_name:
            return

        # remove from the local configuration
        self.removed_conf.append(name_conf_to_delete)
        self.config.remove_section(name_conf_to_delete)

        # remove from the global list
        if name_conf_to_delete in self.list_conf:
            self.list_conf.remove(name_conf_to_delete)

        # remove from de list box widget
        self.list_box_conf.Delete(item)
        new_item = 0 if item == 0 else item - 1

        # select the previous one
        self.list_box_conf.Select(new_item)

        # refresh the text fields
        self.refresh_param_upload(self.list_box_conf.GetClientData(new_item))

    def on_default_conf(self, _):
        item = self.list_box_conf.GetSelection()
        self.default_section_name = self.list_box_conf.GetClientData(item)
        self.refresh_list_conf()
        self.list_box_conf.Select(item)

    def on_save_conf(self, _):
        item = self.list_box_conf.GetSelection()
        name_conf_to_save = self.list_box_conf.GetClientData(item)
        self.config[name_conf_to_save]['name'] = self.conf_name.GetValue()
        self.config[name_conf_to_save]['user'] = self.upload_user.GetValue()
        self.config[name_conf_to_save]['password'] = self.upload_password.GetValue()
        self.config[name_conf_to_save]['url'] = self.upload_url.GetValue()
        self.config[name_conf_to_save]['group'] = self.upload_group.GetValue()
        self.config[name_conf_to_save]['publish'] = str(
            self.upload_publish.GetValue())
        self.config[name_conf_to_save]['author'] = self.upload_author.GetValue()
        self.refresh_list_conf()
        self.list_box_conf.Select(item)
        self.change = False

    def refresh_list_conf(self):
        self.list_box_conf.Clear()
        for conf in self.list_conf:
            name = ""
            if self.default_section_name == conf:
                name = self.config[conf]['name'] + ' (default)'
            else:
                name = self.config[conf]['name']

            self.list_box_conf.Insert(name, self.list_box_conf.Count, conf)

    def refresh_param_upload(self, item=None):
        if item is None:

            if 'default_conf' in self.config[commons.UPLOAD]:
                self.default_section_name = self.config[commons.UPLOAD]['default_conf']

            self.conf_name.SetValue(
                self.config[self.default_section_name]['name'])
            self.upload_user.SetValue(
                self.config[self.default_section_name]['user'])
            self.upload_password.SetValue(
                self.config[self.default_section_name]['password'])
            self.upload_url.SetValue(
                self.config[self.default_section_name]['url'])
            self.upload_group.SetValue(
                self.config[self.default_section_name]['group'])
            self.upload_publish.SetValue(commons.str2bool(
                self.config[self.default_section_name]['publish']))
            self.upload_author.SetValue(
                self.config[self.default_section_name]['author'])
        else:
            self.conf_name.SetValue(self.config[item]['name'])
            self.upload_user.SetValue(self.config[item]['user'])
            self.upload_password.SetValue(self.config[item]['password'])
            self.upload_url.SetValue(self.config[item]['url'])
            self.upload_group.SetValue(self.config[item]['group'])
            self.upload_publish.SetValue(
                commons.str2bool(self.config[item]['publish']))
            self.upload_author.SetValue(self.config[item]['author'])

    def on_ok(self, event):
        del event

        if self.change:
            answer = wx.MessageBox(
                "La configuration a été modifié, voulez vous la sauvegarder avant de quitter ?",
                caption="Sauvegarder", style=wx.YES_NO | wx.CANCEL | wx.CENTRE)

            if(answer == wx.CANCEL):
                return
            elif answer == wx.YES:
                self.on_save_conf(None)

        if self.use_jira.GetValue():
            j = jira_connection.JiraConnection()
            j.url = self.jira_url.GetValue()
            j.user = self.jira_user.GetValue()
            j.password = self.jira_password.GetValue()
            try:
                j.get_connector()
            except JIRAError as ex:
                self.use_jira.SetValue(False)
                if ex.status_code == 401:
                    wx.MessageBox(
                        "error while connecting to Jira : Bad credentials",
                        "Jira", wx.OK | wx.ICON_ERROR)
                elif ex.status_code == 404:
                    wx.MessageBox(
                        "error while connecting to Jira, 404 not found.  {url}".format(
                            url=ex.url),
                        "Jira", wx.OK | wx.ICON_ERROR)
                else:
                    wx.MessageBox("error while connecting to Jira : \n%s" % (
                        ex), "Jira", wx.OK | wx.ICON_ERROR)
            except ConnectionError as ex:
                self.use_jira.SetValue(False)
                wx.MessageBox("error while connecting to Jira : \n%s" % (
                    ex), "Jira", wx.OK | wx.ICON_ERROR)

        # sauvegarde des parametres Jira
        new_config = []

        # JIRA
        new_config += [
            {'module': commons.JIRA, 'key': 'user',
             'value': self.jira_user.GetValue()},
            {'module': commons.JIRA, 'key': 'password',
             'value': self.jira_password.GetValue()},
            {'module': commons.JIRA, 'key': 'url',
             'value': self.jira_url.GetValue()},
            {'module': commons.JIRA, 'key': 'use_jira',
             'value': str(self.use_jira.GetValue())}]

        # GENERATE_PACKAGE
        new_config += [
            {'module': commons.GENERATE_PACKAGE, 'key': 'user',
             'value': self.package_user.GetValue()},
            {'module': commons.GENERATE_PACKAGE, 'key': 'password',
             'value': self.package_password.GetValue()},
            {'module': commons.GENERATE_PACKAGE, 'key': 'url',
             'value': self.package_url.GetValue()}]

        # REGENT
        new_config += [
            {'module': commons.REGENT, 'key': 'user',
             'value': self.regent_user.GetValue()},
            {'module': commons.REGENT, 'key': 'password',
             'value': self.regent_password.GetValue()},
            {'module': commons.REGENT, 'key': 'url',
             'value': self.regent_url.GetValue()}]

        # UPLOAD
        new_config += [
            {'module': commons.UPLOAD, 'key': 'user',
             'value': self.config[self.default_section_name]['user']},
            {'module': commons.UPLOAD, 'key': 'password',
             'value': self.config[self.default_section_name]['password']},
            {'module': commons.UPLOAD, 'key': 'url',
             'value': self.config[self.default_section_name]['url']},
            {'module': commons.UPLOAD, 'key': 'group',
             'value': self.config[self.default_section_name]['group']},
            {'module': commons.UPLOAD, 'key': 'publish',
             'value': self.config[self.default_section_name]['publish']},
            {'module': commons.UPLOAD, 'key': 'author',
             'value': self.config[self.default_section_name]['author']},
            {'module': commons.UPLOAD, 'key': 'list_conf',
             'value': ",".join(self.list_conf)},
            {'module': commons.UPLOAD, 'key': 'default_conf',
             'value': self.default_section_name}]

        for conf in self.list_conf:
            new_config += [
                {'module': conf, 'key': 'name',
                 'value': self.config[conf]['name']},
                {'module': conf, 'key': 'user',
                 'value': self.config[conf]['user']},
                {'module': conf, 'key': 'password',
                 'value': self.config[conf]['password']},
                {'module': conf, 'key': 'url',
                 'value': self.config[conf]['url']},
                {'module': conf, 'key': 'group',
                 'value': self.config[conf]['group']},
                {'module': conf, 'key': 'author',
                 'value': self.config[conf]['author']},
                {'module': conf, 'key': 'publish',
                 'value': self.config[conf]['publish']},
            ]
        commons.set_conf(new_config, self.removed_conf)

        self.parent.Destroy()
    ###########################################################################

    def init_field(self):
        self.config = commons.get_conf()

        # jira
        self.jira_user.SetValue(self.config[commons.JIRA]['user'])
        self.jira_password.SetValue(self.config[commons.JIRA]['password'])
        self.jira_url.SetValue(self.config[commons.JIRA]['url'])
        self.use_jira.SetValue(commons.str2bool(
            self.config[commons.JIRA]['use_jira']))

        # upload
        if 'list_conf' in self.config[commons.UPLOAD]:
            self.list_conf = self.config[commons.UPLOAD]['list_conf'].replace(
                " ", "").split(",")
        else:
            self.list_conf = ["UPLOAD_CONF1"]
            self.default_section_name = 'UPLOAD_CONF1'
            self.config[self.default_section_name] = {}
            self.config[self.default_section_name]['name'] = "my_configuration"
            self.config[self.default_section_name]['user'] = self.config[commons.UPLOAD]['user']
            self.config[self.default_section_name]['password'] = self.config[commons.UPLOAD]['password']
            self.config[self.default_section_name]['url'] = self.config[commons.UPLOAD]['url']
            self.config[self.default_section_name]['group'] = self.config[commons.UPLOAD]['group']
            self.config[self.default_section_name]['publish'] = self.config[commons.UPLOAD]['publish']
            self.config[self.default_section_name]['author'] = self.config[commons.UPLOAD]['author']

        self.refresh_param_upload()
        self.refresh_list_conf()

        # package
        self.package_user.SetValue(
            self.config[commons.GENERATE_PACKAGE]['user'])
        self.package_password.SetValue(
            self.config[commons.GENERATE_PACKAGE]['password'])
        self.package_url.SetValue(self.config[commons.GENERATE_PACKAGE]['url'])

        # regent
        self.regent_user.SetValue(self.config[commons.REGENT]['user'])
        self.regent_password.SetValue(self.config[commons.REGENT]['password'])
        self.regent_url.SetValue(self.config[commons.REGENT]['url'])

class PreferencesDialog(wx.Dialog):
    def __init__(self, parent, title="title"):
        super(PreferencesDialog, self).__init__(
            parent, title=title,
            style=wx.DEFAULT_DIALOG_STYLE | wx.RESIZE_BORDER)

        self.min_width = 300
        self.min_height = 200
        self.width = 500
        self.height = 500
        self.SetSize(wx.Size(self.width, self.height))
        self.SetMinClientSize(wx.Size(self.min_width, self.min_height))
        MainPanel(self)

        # affiche la fenetre d'inspection
        # wx.lib.inspection.InspectionTool().Show()

###############################################################################
# main script
###############################################################################
def main():
    app = wx.App()
    frm = PreferencesDialog(None, title='pref_test')
    frm.Show()
    app.MainLoop()


if __name__ == '__main__':
    main()
