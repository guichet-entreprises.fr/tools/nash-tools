#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

import wx

from wx.lib.pubsub import pub


class ProgressDialog(wx.Dialog):

    ###########################################################################
    def __init__(self, title, nb_max, msg=""):
        """Constructor"""
        self.width = 530
        self.height = 200
        wx.Dialog.__init__(self, None, title=title)

        self.SetSize(wx.Size(self.width, self.height))
        self.nb_max = nb_max
        self.count = 2

        self.main_sizer = wx.BoxSizer(wx.VERTICAL)

        # message
        self.message_sizer = wx.BoxSizer(wx.VERTICAL)
        self.msg = wx.StaticText(self, label=msg)
        self.msg.Wrap(self.width - 50)
        self.message_sizer.Add(self.msg, 0, wx.ALL | wx.CENTER, 5)

        # progress bar
        self.progress_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.progress = wx.Gauge(self, range=self.nb_max)

        self.progress_sizer.AddStretchSpacer()
        self.progress_sizer.Add(self.progress, 6, wx.EXPAND)
        self.progress_sizer.AddStretchSpacer()

        # boutons
        self.button_sizer = wx.BoxSizer(wx.HORIZONTAL)

        btn_cancel = wx.Button(self, label="Cancel")
        btn_cancel.Bind(wx.EVT_BUTTON, self.on_cancel)

        self.button_sizer.Add(btn_cancel, 0, wx.ALL | wx.CENTER, 5)

        self.main_sizer.Add(self.message_sizer, 0, wx.ALL | wx.CENTER, 5)
        self.main_sizer.Add(self.progress_sizer, 0, wx.EXPAND, 5)
        self.main_sizer.Add((0, 0), 1, wx.ALL | wx.CENTER)
        self.main_sizer.Add(self.button_sizer, 0, wx.ALL | wx.CENTER, 5)

        self.SetSizer(self.main_sizer)

        self.Bind(wx.EVT_CLOSE, self.on_cancel)

        # pubsub listener
        pub.subscribe(self.update_progresss, "update_progress")
        pub.subscribe(self.destroy_progress, "destroy_progress")

    ###########################################################################
    def update_progresss(self, msg="", increment=None):
        """
        Update the progress bar
        """

        if next is not None:
            self.count += increment

        if msg is not "":
            self.msg.SetLabel(msg)
            self.msg.Wrap(self.width - 50)
            self.main_sizer.Layout()

        self.progress.SetValue(self.count)

        if self.count >= self.nb_max:
            self.Destroy()

    ###########################################################################
    def destroy_progress(self):
        self.Destroy()

    ###########################################################################
    def on_cancel(self, event):
        del event
        pub.sendMessage("cancel_progress")
        self.msg.SetLabel("canceling...")
        self.msg.Wrap(self.width - 50)
        self.main_sizer.Layout()

    def on_destroy(self, event):
        del event
        self.Destroy()
