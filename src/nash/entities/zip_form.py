#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import os
import shutil
import zipfile
import logging

from nash.services import commons
from nash.entities import form


class ZipForm(form.Form):

    def __init__(self, filename, load_form=True):
        super().__init__(filename, load_form=load_form)

    def get_form_name(self):
        pathtmp = self.get_filename().split(os.sep)
        return pathtmp[-1]

    def read(self):
        """Read the standalone form"""
        if not os.path.isfile(self.get_filename()):
            raise "This is not a file : " + self.get_filename()

        with zipfile.ZipFile(self.get_filename(), 'r') as zf_read:
            list_name = zf_read.namelist()
            if not self.check_description(list_name):
                self.inconsistent = True
                return

            for file_name in list_name:
                logging.debug('read the file %s', file_name)
                data = zf_read.read(file_name)
                self.add_content(file_name, data)

        self.zip_filename = self.get_filename()

    def check_description(self, list_name):
        return "description.xml" in list_name

    def load(self):
        with zipfile.ZipFile(self.get_filename(), 'r') as zf_read:
            list_name = zf_read.namelist()
            if not self.check_description(list_name):
                return

            for file_name in list_name:
                logging.debug('read the file %s', file_name)
                data = zf_read.read(file_name)
                self.add_content(file_name, data)
            self.status = form.STATUS_LOADED

    def build(self):
        pass
        
    def clean(self):
        pass


    def rename_zip(self, new_path):
        shutil.move(self.get_zip_filename(), new_path)
        self.set_zip_filename(new_path)

    def write(self, build=False, filename=None, full_save=False):
        """Write the standalone form"""

        the_filename = self.get_filename()
        if filename is not None:
            the_filename = commons.set_correct_path(filename)

        logging.info('write the file %s', the_filename)

        zf_write = zipfile.ZipFile(the_filename, 'w')
        for filename in self.get_files_list():
            logging.debug('write the file %s', filename)
            zf_write.writestr(filename, self[filename])

        zf_write.close()

        logging.debug('end of write the file %s', the_filename)
        self.set_filename(the_filename)
