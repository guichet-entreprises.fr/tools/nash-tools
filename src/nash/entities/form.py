#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import os
import logging
from abc import ABCMeta, abstractmethod
from bs4 import BeautifulSoup

import krypton

from nash.services import commons
from nash.services import selenese
from nash.services import jira_connection
from nash.services.exceptions import BusinessException


__KEY_DESCRIPTION_XML__ = 'description.xml'
STATUS_NOT_LOADED = 0
STATUS_LOADED = 1

PLACE_END = -1
PLACE_BEGIN = 0


class Form(metaclass=ABCMeta):

    def __init__(self, filename, load_form=True):
        self.filename = filename
        self.zip_filename = None
        self.issue = None
        self.group = None
        self.inconsistent = False
        self.__files__ = {}
        self.status = STATUS_NOT_LOADED
        if self.read() is None:
            return

        if load_form:
            self.load()

    @abstractmethod
    def read(self):
        pass

    @abstractmethod
    def load(self):
        pass

    @abstractmethod
    def build(self):
        pass

    @abstractmethod
    def rename_zip(self, new_path):
        pass

    @abstractmethod
    def write(self, build=False, filename=None, full_save=False):
        pass

    def set_issue(self, issue):
        self.issue = issue

    def get_issue(self):
        return self.issue

    def set_group(self, group):
        self.group = group

    def get_group(self):
        return self.group

    def set_filename(self, filename):
        self.filename = commons.set_correct_path(filename)
        logging.debug('standalone_form filename = %s', self.filename)

    def get_filename(self):
        return self.filename

    def add_content(self, filename, data):
        self.__files__[filename] = data

    def __getitem__(self, key):
        return self.__files__[key]

    def __setitem__(self, key, value):
        self.__files__[key] = value

    def __len__(self):
        return len(self.__files__)

    def get_files(self):
        return self.__files__

    def get_files_list(self):
        return self.__files__.keys()

    def set_zip_filename(self, zip_filename):
        self.zip_filename = zip_filename

    def get_zip_filename(self, build=False):
        del build
        return self.zip_filename

    def get_description_xml(self):
        key_result = None
        for key in self.__files__:
            if key.endswith(__KEY_DESCRIPTION_XML__):
                key_result = key
        if key_result is None:
            raise "The standalone form does not have any description.xml"
        return BeautifulSoup(self[key_result], "xml")

    def get_description_xml_raw(self):
        key_result = None
        for key in self.__files__:
            if key.endswith(__KEY_DESCRIPTION_XML__):
                key_result = key
        if key_result is None:
            raise "The standalone form does not have any description.xml"
        return self[key_result]

    def set_description_xml(self, data):
        for key in self.__files__:
            if key.endswith(__KEY_DESCRIPTION_XML__):
                self[key] = data
                return

    def get_title(self):
        return self.get_description_xml().description.title.string

    def set_title(self, title):
        desc = self.get_description_xml()
        desc.description.title.string = title
        self.set_description_xml(str(desc))

    def get_description(self):
        return self.get_description_xml().description.description.string

    def set_description(self, description):
        desc = self.get_description_xml()
        desc.description.description.string = description
        self.set_description_xml(str(desc))

    def get_steps(self):
        desc = self.get_description_xml()
        xml_steps = desc.description.steps.find_all('step')
        return [s.attrs for s in xml_steps]

    def get_referentials(self):
        result = []
        desc = self.get_description_xml()
        if desc.description.referentials is not None:
            xml_referentials = desc.description.referentials.find_all(
                'referential')
            result = [s.text for s in xml_referentials]
        return result

    def get_translations(self):
        result = []
        desc = self.get_description_xml()
        if desc.description.translations is not None:
            xml_translation = desc.description.translations.find_all('for')
            result = [s.text for s in xml_translation]
        return result

    def get_form_uid(self):
        desc = self.get_description_xml()
        if desc.description.find('form-uid') is not None:
            form_uid = desc.description.find('form-uid').text
        else:
            form_uid = None
        return form_uid

    def set_form_uid(self, form_uid):
        desc = self.get_description_xml()
        tag_form_uid = desc.description.find("form-uid")
        if tag_form_uid is None:
            tag_form_uid = desc.new_tag('form-uid')
            desc.description.find('reference-id').insert_after(tag_form_uid)
            desc.description.find('reference-id').insert_after("\n")
        tag_form_uid.string = form_uid
        self.set_description_xml(str(desc))

    def get_reference_id(self):
        desc = self.get_description_xml()
        reference_id = desc.description.find('reference-id').text
        return reference_id

    def is_profiler(self):
        if "profiler" in self.get_reference_id().lower():
            return True
        return False

    def get_form_name(self):
        pass

    def convert_to_selenese(self):
        result = []
        test_case = krypton.TestCase()
        steps = self.get_steps()

        test_case += krypton.testCaseSeparator()
        test_case += krypton.testCaseComment("Start the form %s" %
                                             (self.get_title()))
        test_case += krypton.testCaseSeparator()

        nb_steps = len(steps)
        count = 1
        for step in steps:
            count = count + 1
            test_case.setName(step['id'])
            test_case += krypton.testCaseSeparator()
            test_case += krypton.testCaseComment(
                "Start the step %d / %d : id=%s" % (count,
                                                    nb_steps, step['id']))
            test_case += krypton.testCaseSeparator()
            try:
                test_case += selenese.convert_data_xml_to_selenese(
                    self[step['data']], self.is_profiler())
            except KeyError:
                logging.error('data file not present for step %s', step['id'])
            result.append(test_case)
            test_case = krypton.TestCase()

        return result

    def set_formality_status_jira_done(self):
        j = jira_connection.JiraConnection()

        if self.issue is None:
            self.issue = j.search_one_issues(self.get_form_uid())
            return False

        if self.issue is not None:
            j.set_status_done(self.issue)

        return True

    def create_jira_issue(self, conf_infos, project="SAND",
                          sprint=None, status=None):
        del conf_infos, project, sprint, status
        j = jira_connection.JiraConnection()

        if self.issue is None:
            self.issue = j.search_one_issues(self.get_form_uid())
            return False

        if self.issue is not None:
            j.set_status_done(self.issue)

        return True

    def get_formality_infos(self):

        titre = self.get_title()
        reference_id = self.get_reference_id()
        form_uid = self.get_form_uid()

        result = {'titre': titre,
                  'reference id': reference_id}
        if form_uid is not None:
            result['uID'] = form_uid

        return result

    def get_formality_infos_jira(self):
        issue = self.get_jira_issues()

        result = {}

        if issue is not None:
            result['key JIRA'] = issue.key
            result['summary JIRA'] = issue.fields.summary
            result['status JIRA'] = issue.fields.status.name
            if issue.fields.assignee is None:
                result['assigné à'] = "nobody"
            else:
                result['assigné à'] = issue.fields.assignee.displayName

        return result

    def get_jira_issues(self):
        j = jira_connection.JiraConnection()
        issue = None
        try:
            issue = j.search_one_issues(self.get_form_uid())
        except BusinessException as err:
            logging.debug(err)

        return issue

    def __str__(self):
        result = ""
        result += "Title            : %s\n" % (self.get_title())
        result += "Description		: %s\n" % (self.get_description())
        result += "Standalone filename    : %s\n" % (self.get_filename())
        result += "Number of files        : %s\n" % (len(self))
        result += "Form-UID         : %s\n" % (self.get_form_uid())
        result += "loaded    		: %s\n" % (self.status)
        count = 0
        for the_file in self.get_files_list():
            result += "%5d : %s  --> %s\n" % (count,
                                              repr(the_file),
                                              repr(self[the_file])[0:30])
            count += 1
        return result

    def get_steps_id(self):
        result = []
        for step in self.get_steps():
            result.append(step['id'])
        return result

    def merge_form(self, form, place=PLACE_END):

        description_to_merge = form.get_description_xml()
        description = self.get_description_xml()
        form_directory = os.path.split(form.get_filename())[0]
        step_to_merge = [os.path.join(form_directory, os.path.dirname(
            s['data'])) for s in form.get_steps()]
        # referentials = form.get_referentials()
        # translations = form.get_translations()

        # merge description.xml
        for child in description_to_merge.description.steps.findChildren():
            # si la formalité a déja une step avec le même id
            if child['id'] in self.get_steps_id():
                pass
            else:
                description.description.steps.insert(place, child)
                child.insert_before("\n")

        self.set_description_xml(str(description))

        # merge files
        for dirname in step_to_merge:
            step_folder = os.path.basename(dirname)
            for filename in os.listdir(dirname):
                new_filename = os.path.join(os.path.split(self.get_filename())[
                    0], step_folder, os.path.basename(
                        filename))
                self.add_content(new_filename, commons.get_file_content(
                    os.path.join(dirname, filename)))
