#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################
import os
import shutil
import codecs
import zipfile

from itertools import chain

from nash.services import commons
from nash.entities import form


class XmlForm(form.Form):

    def __init__(self, filename, load_form=True):
        super().__init__(filename, load_form=load_form)
        self.status = form.STATUS_NOT_LOADED

    def get_form_name(self):
        pathtmp = self.get_filename().split(os.sep)
        formality_name = pathtmp[len(pathtmp) - 2]
        return formality_name

    def read(self):
        """Read the standalone form"""
        data = commons.get_file_content(self.filename)
        self.add_content(self.filename, data)
        self.set_filename(self.filename)

    def load(self):
        description_xml_file = self.get_filename()
        working_directory = os.path.split(description_xml_file)[0]
        paths = [os.path.join(working_directory, os.path.dirname(
            s['data'])) for s in self.get_steps()]
        referentials = self.get_referentials()
        translations = self.get_translations()

        paths.extend([os.path.join(working_directory, "css")])
        for path, _, files in chain.from_iterable(
                os.walk(path) for path in paths):
            for file in files:
                filename = os.path.join(path, file)
                self.add_content(filename, commons.get_file_content(filename))

        for ref in referentials:
            filename = os.path.join(working_directory, ref)
            self.add_content(filename, commons.get_file_content(filename))

        for translation in translations:
            filename = os.path.join(working_directory, translation)
            self.add_content(filename, commons.get_file_content(filename))

        self.status = form.STATUS_LOADED

    def write(self, build=False, filename=None, full_save=False):

        # écriture du description.xml
        writer = codecs.open(self.get_filename(),
                             encoding='utf-8', mode='wb')

        writer.write(self.get_description_xml_raw())
        writer.close()

        if form.STATUS_LOADED and full_save:
            for filename in self.get_files_list():
                if not os.path.exists(os.path.split(filename)[0]):
                    os.mkdir(os.path.split(filename)[0])
                writer = codecs.open(filename, encoding='utf-8', mode='wb')

                writer.write(self[filename])
                writer.close()

        if build:
            self.build()

    def get_zip_filename(self, build=False):
        if build:
            self.build()
        return self.zip_filename

    def rename_zip(self, new_path):
        shutil.move(self.get_zip_filename(), new_path)
        self.set_zip_filename(new_path)

    def clean(self):
        os.remove(self.get_zip_filename())

    def build(self):
        self.status = form.STATUS_LOADED
        description_xml_file = self.get_filename()
        working_directory = os.path.split(description_xml_file)[0]
        form_name = os.path.basename(working_directory)
        self.set_zip_filename(os.path.join(
            working_directory, form_name) + ".zip")
        paths = [os.path.join(working_directory, os.path.dirname(
            s['data'])) for s in self.get_steps()]
        paths = list(set(paths))
        referentials_to_write = self.get_referentials()
        translations_to_write = self.get_translations()

        try:
            os.remove(self.get_zip_filename())
        except FileNotFoundError:
            pass

        zipf = zipfile.ZipFile(self.get_zip_filename(),
                               'w', zipfile.ZIP_DEFLATED)
        # usual directory
        if os.path.isdir(os.path.join(working_directory, "css")):
            paths.extend([os.path.join(working_directory, "css")])

        if os.path.isdir(os.path.join(working_directory, "js")):
            paths.extend([os.path.join(working_directory, "js")])

        if os.path.isdir(os.path.join(working_directory, "img")):
            paths.extend([os.path.join(working_directory, "img")])

        for path, _, files in chain.from_iterable(
                os.walk(path) for path in paths):
            for file in files:
                if(file != os.path.basename(self.get_zip_filename())
                   and file != "description.xml"):
                    zipf.write(os.path.join(path, file), os.path.join(
                        path.replace(working_directory + os.sep, ''), file))

        for ref in referentials_to_write:
            zipf.write(os.path.join(working_directory, ref), ref)

        for translation in translations_to_write:
            zipf.write(os.path.join(
                working_directory, translation), translation)

        # description.xml
        zipf.write(description_xml_file, "description.xml")

        # history
        history_file = os.path.join(working_directory, "history.xml")
        if os.path.isfile(history_file):
            zipf.write(history_file, "history.xml")

        # roles
        roles_file = os.path.join(working_directory, "roles.xml")
        if os.path.isfile(roles_file):
            zipf.write(roles_file, "roles.xml")

        # readme
        readme_file = os.path.join(working_directory, "README.md")
        if os.path.isfile(readme_file):
            zipf.write(readme_file, "README.md")

        # meta
        meta_file = os.path.join(working_directory, "meta.xml")
        if os.path.isfile(meta_file):
            zipf.write(meta_file, "meta.xml")

        zipf.close()
