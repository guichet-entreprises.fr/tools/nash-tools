﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
#     All Rights Reserved.
#     Unauthorized copying of this file, via any medium is strictly prohibited
#     Dissemination of this information or reproduction of this material
#     is strictly forbidden unless prior written permission is obtained
#     from Guichet Entreprises.
###############################################################################

from threading import Thread

from wx.lib.pubsub import pub

from nash.services import commons
from nash.services import standalone_form


class ServiceThread(Thread):

    nb_formality = 0
    list_forms = None
    conf_infos = None
    action = None

    def __init__(self, action, nb_formality, list_forms,
                 conf_infos=None, params=None):
        Thread.__init__(self)

        self.nb_formality = nb_formality
        self.list_forms = list_forms
        self.conf_infos = conf_infos
        self.action = action
        self.params = params
        self.count = 0
        self.result = []
        self.form = None

        pub.subscribe(self.on_cancel, "cancel_progress")

        self.cancel = False

        self.start()    # start the thread

    def run(self):
        """Run Worker Thread."""

        for form in self.list_forms:
            self.form = form
            if self.cancel:
                pub.sendMessage("destroy_progress")
                break
            self.count += 1

            try:
                self.actions[self.action](self)

            except Exception as ex:
                pub.sendMessage("result_progress",
                                action="ERROR", result=str(ex))
                pub.sendMessage("destroy_progress")
                return

            pub.sendMessage("update_progress", increment=10)

        pub.sendMessage("result_progress", action=self.action,
                        result=self.result)
        # pub.sendMessage("destroy_progress")

    def on_cancel(self):
        self.cancel = True

    def upload(self):
        msg = "Uploading ({num_form}/{nb_formality}) {form} on {url}\n" \
            "please wait...".format(
                num_form=self.count, nb_formality=len(self.list_forms),
                form=self.form.get_form_name(),
                url=self.conf_infos['url'])
        pub.sendMessage("update_progress", msg=msg, increment=0)
        standalone_form.upload_standalone_form(self.form, self.conf_infos)
        self.result.append(self.form)

    def purge(self):
        msg = "purge ({num_form}/{nb_formality}) {form} on {url}\n" \
            "please wait...".format(
                num_form=self.count, nb_formality=len(self.list_forms),
                form=self.form.get_form_name(),
                url=self.conf_infos['url'])
        pub.sendMessage("update_progress", msg=msg, increment=0)
        standalone_form.purge_remote_server(self.form, self.conf_infos)
        self.result.append(self.form)

    def string_externalization(self):
        msg = "Externalization ({num_form}/{nb_formality}) {form}\n" \
            "please wait...".format(
                num_form=self.count, nb_formality=len(self.list_forms),
                form=self.form.get_form_name())
        pub.sendMessage("update_progress", msg=msg, increment=0)
        standalone_form.generate_prepare_translate(self.form)
        self.form.write(full_save=True)
        self.result.append(self.form)

    def regent(self):
        version = self.params['version']
        events = self.params['events']

        list_events_to_print = '[%s]' % ', '.join(map(str, events))

        msg = "Generating Regent ({num_form}/{nb_formality}) {form} "\
            "version regent : {version}, envents : {events}\n"\
            "please wait...".format(
                num_form=self.count, nb_formality=len(self.list_forms),
                form=self.form.get_form_name(),
                version=version, events=list_events_to_print)

        pub.sendMessage("update_progress", msg=msg, increment=0)
        standalone_form.generate_regent(
            self.form, self.conf_infos, version, events)
        self.result.append(self.form)

    def create_jira_issue(self):
        # project = self.params['project']
        # sprint = self.params['sprint']
        # status = self.params['status']

        msg = "Creating Jira issue ({num_form}/{nb_formality}) "\
              "{form} : {formuid} on \n"\
              "please wait...".format(num_form=self.count,
                                      nb_formality=len(
                                          self.list_forms),
                                      form=self.form.get_form_name(),
                                      formuid=self.form.get_form_uid())
        pub.sendMessage("update_progress", msg=msg, increment=0)
        # standalone_form.create_jira_issue(
        #     self.form, self.conf_infos, project, sprint, status)
        self.result.append(self.form)

    # liste des actions disponibles
    actions = {commons.UPLOAD: upload,
               commons.PURGE: purge,
               commons.REGENT: regent,
               commons.STRING_EX: string_externalization,
               commons.CREATE_JIRA_ISSUE: create_jira_issue}
